<?php 
/*======================================================================
 変数の初期化
======================================================================*/
$page_flag = 0;
$clean = array();

/*======================================================================
 悪意のある入力を無効化する
======================================================================*/
function sethtmlspecialchars($data) {
    //データが配列の場合
    if (is_array($data)) {
      return array_map("sethtmlspecialchars", $data);

    //データが配列ではない場合
    } else {
      return htmlspecialchars($data, ENT_QUOTES);
    }
}

if( !empty($_POST) ) {
    $_POST = sethtmlspecialchars($_POST);
    $clean = $_POST;
}

/*======================================================================
 項目の設定
======================================================================*/
$business_categories = array("","社労士","税理士","一般企業");
$company_capitals = array("","１億円超","１億円以下","わからない");
$roumu_periods = array("","月","1年","3年","5年");
$inflow_sources = array(
    'tv' => 'テレビ広告',
    'video' => '動画広告',
    'newspaper' => '新聞広告',
    'magazine' => '雑誌広告',
    'digital-newspaper' => 'デジタル系新聞',
    'transportation' => '交通広告',
    'seminar' => 'セミナー',
    'web' => 'WEB広告',
    'exhibition' => '展示会等',
    'dm-by-email' => 'メールDM',
    'dm-by-post' => '郵送DM',
    'internet' => 'インターネットで検索して',
    'tel-from-salesman' => '営業担当からの電話',
    'press-release' => 'プレスリリース',
    'sns' => 'SNS',
    'none' => '印象に残った広告はない',
    'other' => 'その他',
);
$ads = array(
    'tv' => 'テレビ広告',
    'newspaper' => '新聞広告',
    'magazine' => '雑誌広告',
    'digital-newspaper' => 'デジタル系新聞',
    'transportation' => '交通広告',
    'seminar' => 'セミナー',
    'web' => 'WEB広告',
    'exhibition' => '展示会等',
    'dm-by-post' => '郵送DM',
    'dm-by-email' => 'メールDM',
    'none' => '印象に残った広告はない',
    'other' => 'その他',
);
$products = array(
    'roumu' => '労務',
    'roumu-lite' => '労務ライト',
    'nencho' => '年末調整',
    'kyuyomeisai' => 'Web給与明細',
    'yukyu' => '有休管理',
    'mynumber' => 'マイナンバー',
);

/*======================================================================
 Google reCAPTCHA
======================================================================*/
// シークレットキー
if ( $_SERVER['SERVER_NAME'] == "www.officestation.jp" ) {
    $secret_key = '6LdRme0UAAAAAE27QRwxosVV1UQtdEtl4Snfco6l';
} elseif ( $_SERVER['SERVER_NAME'] == "dev.www-officestation-jp.mnsta.com" ) {
    $secret_key = '6Lf4me0UAAAAAJv3HN0ggaWiOMqPTDH4l0_-TUfL';
} elseif ( $_SERVER['SERVER_NAME'] == "www-officestation-jp.infrastructure.mnsta.com" ) {
    $secret_key = '6LfbAPcUAAAAANGlqxgX1aMF2ZtOZkVRpjyzO1-A';
}

//サイトキー
if ( $_SERVER['SERVER_NAME'] == "www.officestation.jp" ) {
    $site_key = '6LdRme0UAAAAAIxWFaz8UKrxpdpJyc-fFwnbOXBT';
} elseif ( $_SERVER['SERVER_NAME'] == "dev.www-officestation-jp.mnsta.com" ) {
    $site_key = '6Lf4me0UAAAAAIvU2z46qNz65qG1ut3WWLWFnE92';
} elseif ( $_SERVER['SERVER_NAME'] == "www-officestation-jp.infrastructure.mnsta.com" ) {
    $site_key = '6LfbAPcUAAAAAAANx0HtIGk9NxYBPplJ4LtOt-LP';
}

/*======================================================================
 「確認する」ボタンが押下されたとき
======================================================================*/
if( !empty($_POST['btn_confirm']) ) {

    //従業員数：全角数字を半角に変換
    if( isset($clean['number_of_employees']) && !empty($clean['number_of_employees']) ) {
        $clean['number_of_employees'] = mb_convert_kana( $clean['number_of_employees'], 'n' );
    }

    //サーバー側でのバリデーションチェック
    //validationメソッド呼び出し
    $error = validation($clean);

	if( empty($error) ) {
        // エラーがない場合は確認画面に遷移
        $page_flag = 1;

        //従業員数：0埋めとなっている場合の不要な0を削除（例：000100 → 100 にデータ加工）
        if( isset($clean['number_of_employees']) && !empty($clean['number_of_employees']) ) {
            $clean['number_of_employees'] = ltrim($clean['number_of_employees'], 0);
        }
    } else {
        // エラーがある場合は確認画面に遷移させない
        $page_flag = 0;
    }

} else {
    $page_flag;
}

/*======================================================================
 バリデーション（エラー）チェックの実行処理内容
======================================================================*/
function validation($data) {

    $error = array();
    $reg_str = "/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/";

    // 業態のバリデーション
    if( isset($data['business_category']) && empty($data['business_category']) ){
		$error[] = "「業態」は必ず選択してください。";
    }

	// 姓のバリデーション
	if( isset($data['last_name']) && empty($data['last_name']) ) {
		$error[] = "「姓」は必ず入力してください。";
    }

	// 名のバリデーション
	if( isset($data['first_name']) && empty($data['first_name']) ) {
		$error[] = "「名」は必ず入力してください。";
    }
    
	// 会社名のバリデーション
	if( isset($data['user_company']) && empty($data['user_company']) ) {
		$error[] = "「会社名」は必ず入力してください。";
    }

    if( !isset($data['business_category']) || $data['business_category'] == "一般企業" ) {
        // 資本金のバリデーション
        if( isset($data['company_capital']) && empty($data['company_capital']) ) {
            $error[] = "「資本金」は必ず選択してください。";
        }
        // 従業員数のバリデーション
        if( isset($data['number_of_employees']) ) {
            if( $data['number_of_employees'] === "" ) {
                $error[] = "「従業員数」は必ず入力してください。";
            }elseif ( preg_match('/^[0-9]+$/', $data['number_of_employees']) !== 1 ) {
                $error[] = "「従業員数」は数字で入力してください。";
            } elseif ( $data['number_of_employees'] < 1 ) {
                $error[] = "「従業員数」は1以上で入力してください。";
            } 
        }
    }

	// メールアドレスのバリデーション
	if( isset($data['user_mail']) && empty($data['user_mail']) ) {
		$error[] = "「メールアドレス」は必ず入力してください。";
    }
    
    if (!preg_match($reg_str, $data['user_mail'])) {
        $error[] = "正しい形式のメールアドレスを入力してください。";
    }

	// 電話番号のバリデーション
	if( isset($data['user_tel']) && empty($data['user_tel']) ) {
		$error[] = "「電話番号」は必ず入力してください。";
    }

	// お問い合わせ内容のバリデーション
	if( isset($data['user_message']) && empty($data['user_message']) ) {
		$error[] = "「お問い合わせ内容」は必ず入力してください。";
    }

    // 利用規約同意のバリデーション
	if( isset($data['terms_agree']) && empty($data['terms_agree']) ) {
		$error[] = "利用規約「同意する」に必ずチェックしてください。";
    }

	// プライバシー同意のバリデーション
	if( isset($data['agree']) && empty($data['agree']) ) {
		$error[] = "プライバシーステートメント「同意する」に必ずチェックしてください。";
    }

    // お問い合わせ製品のバリデーション
    if( isset($data['con_product_check']) && empty($data['product']) ) {
		$error[] = "「お問い合わせ製品」には必ず1つ以上の製品にチェックをつけてください。";
    }

    // オフィスステーションシリーズで興味のある製品のバリデーション
    if( isset($data['doc_product_check']) && empty($data['product']) ) {
		$error[] = "「オフィスステーションシリーズで興味のある製品」には必ず1つ以上の製品にチェックをつけてください。";
    }

    /*======================================================================
    Google reCAPTCHAのエラーチェック
    ======================================================================*/    
    // g-recaptcha-responseのデータが送られていなかったら、つまりロボットではありませんにチェックされていなかったらエラーとして扱う
    if( !isset( $_POST['g-recaptcha-response'] ) ){
        $_POST['g-recaptcha-response'] = '' ;
    }

    // シークレットキー
    if ( $_SERVER['SERVER_NAME'] == "www.officestation.jp" ) {
        $secret_key = '6LdRme0UAAAAAE27QRwxosVV1UQtdEtl4Snfco6l';
    } elseif ( $_SERVER['SERVER_NAME'] == "dev.www-officestation-jp.mnsta.com" ) {
        $secret_key = '6Lf4me0UAAAAAJv3HN0ggaWiOMqPTDH4l0_-TUfL';
    } elseif ( $_SERVER['SERVER_NAME'] == "www-officestation-jp.infrastructure.mnsta.com" ) {
        $secret_key = '6LfbAPcUAAAAANGlqxgX1aMF2ZtOZkVRpjyzO1-A';
    }

    //サイトキー
    if ( $_SERVER['SERVER_NAME'] == "www.officestation.jp" ) {
        $site_key = '6LdRme0UAAAAAIxWFaz8UKrxpdpJyc-fFwnbOXBT';
    } elseif ( $_SERVER['SERVER_NAME'] == "dev.www-officestation-jp.mnsta.com" ) {
        $site_key = '6Lf4me0UAAAAAIvU2z46qNz65qG1ut3WWLWFnE92';
    } elseif ( $_SERVER['SERVER_NAME'] == "www-officestation-jp.infrastructure.mnsta.com" ) {
        $site_key = '6LfbAPcUAAAAAAANx0HtIGk9NxYBPplJ4LtOt-LP';
    }

    // google reCAPTCHAにsecret keyなどを送って、不正なくチェックなどされているかどうかを判断してもらう
    $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$secret_key}&response={$_POST['g-recaptcha-response']}");

    //不正があるかないか、reCAPTCHAが返答してくれる
    $json = json_decode($response,true);

    //認証に成功した場合successにtrueが入力される 
    //上記問題がなかった場合はこの中のプログラムを実行
    if($json["success"] == false){
        $error[] = "私はロボットではありませんにチェックを入れてください。";
    }
    
	return $error;
}
?>