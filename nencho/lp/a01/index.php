<?php
    include '../../../module/form/main.php';
    include '../../../module/form/seminar-nencho.php';
?><!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <title>2020年版完全対応！2020年こそWebでスイスイスマートに！ペーパーレス年末調整入門講座 | オンライン開催</title>
    <link rel="canonical" href="https://www.officestation.jp/nencho/lp/a01/" />
    <meta name="description"  content="本セミナーでは、2020年に新たに発生する年末調整の課題を確認するとともに、年末調整をペーパーレス化するメリットをご案内します。さらに「オフィスステーション年末調整」でペーパーレス化を実現した企業さまの活用事例もご紹介します。年末調整に課題を感じ、ツールの導入をご検討している担当者さまはぜひご参加ください。" />
    <meta property="og:title" content="2020年版完全対応！2020年こそWebでスイスイスマートに！ペーパーレス年末調整入門講座 | オンライン開催" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
    <meta property="og:image" content="https://www.officestation.jp/nencho/lp/a01/img/ogp.png" />
    <meta property="og:image:secure_url" content="https://www.officestation.jp/nencho/lp/a01/img/ogp.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="2020年版完全対応！2020年こそWebでスイスイスマートに！ペーパーレス年末調整入門講座 | オンライン開催" />
    <meta name="twitter:image" content="https://www.officestation.jp/nencho/lp/a01/img/ogp.png" />
    <!-- clear a cache -->
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <link rel="stylesheet" href="/module/form/form.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel='dns-prefetch' href='//s.w.org' />
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Noto+Sans+JP:400,900|Noto+Serif+JP|Roboto+Condensed:700&display=swap" rel="stylesheet">
    <!-- JS Setting-->
    <script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
<!-- End Google Tag Manager -->
    <!-- User Heat Tag -->
    <script type="text/javascript">
        (function(add, cla){window['UserHeatTag']=cla;window[cla]=window[cla]||function(){(window[cla].q=window[cla].q||[]).push(arguments)},window[cla].l=1*new Date();var ul=document.createElement('script');var tag = document.getElementsByTagName('script')[0];ul.async=1;ul.src=add;tag.parentNode.insertBefore(ul,tag);})('//uh.nakanohito.jp/uhj2/uh.js', '_uhtracker');_uhtracker({id:'uhIcAz08lS'});
    </script>
    <!-- End User Heat Tag -->
</head>
<body id="top" class="seminar-nencho">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <header id="header-lp-nencho-seminar">
            <img src="img/logo-nencho.png" class="logo" alt="オフィスステーション 年末調整">
    </header>
    <?php if( $page_flag === 0 ): ?>
        <?php if( empty($_POST['btn_confirm']) ): ?>
            <section class="section-mv">
                <div class="mv-area">
                    <h1>
                        <img id="img-mv-pc" src="img/img-mv.png" alt="令和2年版完全対応！2020年こそWebでスイスイスマートに！ペーパーレス年末調整入門講座 | オンライン開催">
                        <img id="img-mv-sp" src="img/img-mv-sp.png" alt="令和2年版完全対応！2020年こそWebでスイスイスマートに！ペーパーレス年末調整入門講座 | オンライン開催">
                    </h1>
                    <a class="mv-btn" href="#form-bottom">お申込みはコチラ</a>
                </div>
            </section>
            <section class="section-recommended">
                <h2>こういう方にオススメ</h2>
                <ul class="recommended-list">
                    <li>年末調整のペーパーレス化に興味がある方</li>
                    <li>「また年末調整の時期が来たか…」と思ってしまう方</li>
                    <li>今年の税制改正が実務にどう影響を与えるか知りたい方</li>
                </ul>
            </section>
            <section class="section-overview">
                <h2>セミナー概要</h2>
                <div class="text-area">
                    <p>早いもので2020年分の年末調整業務が迫ってきております。<br>
                        そろそろ対応策を検討し始めているご担当者さまも多いのではないでしょうか。</p>
                    <p><span class="bold">数ある行政手続き（税務・労務）の中で"最も大変"として挙がるのが「年末調整業務」</span>であり、さらに2020年は基礎控除の改正や所得金額調整控除の創設などに係る大幅な様式変更もあります。</p>
                    <p>負担の大きい年末調整業務を少しでも改善するために、<span class="bold">「スマホで完結できる年末調整」</span>を検討してみてはいかがでしょうか？<br>
                        年末調整業務における負担の大きな要因は、「紙」による管理。</p>
                    <p><span class="orange">「オフィスステーション 年末調整」であれば、煩雑な紙による手続きを大幅に効率化できます。</span></p>
                    <p>本セミナーでは、2020年に新たに発生する年末調整の課題を確認するとともに、年末調整をペーパーレス化するメリットをご案内します。
                        さらに「オフィスステーション 年末調整」でペーパーレス化を実現した企業さまの活用事例もご紹介します。<br>
                        年末調整に課題を感じ、ツールの導入をご検討中のご担当者さまは、ぜひご参加ください。</p>
                </div>
            </section>
            <section class="section-content">
                <h2>セミナー内容</h2>
                <div class="content-area">
                    <div class="text-box">
                        <ul class="content-list">
                            <li>テレワークを主体とした新しい年末調整のカタチ</li>
                            <li>最新の税制改正とその影響</li>
                            <li>年末調整のペーパーレス化がもたらすメリット</li>
                            <li>「オフィスステーション 年末調整」のご紹介</li>
                            <li>導入企業のご活用事例</li>
                        </ul>
                        <div class="content-infomation">
                            <h3>開催概要</h3>
                            <ul>
                                <li>料金 : 無料</li>
                                <li>対象 : 年末調整業務のご担当者様</li>
                                <li>場所 : オンライン</li>
                                <li>主催 : 株式会社エフアンドエム</li>
                            </ul>
                        </div>
                    </div>
                    <div class="img-box">
                        <img src="img/img-content.png" alt="">
                    </div>
                </div>
            </section>
            <section class="section-schedule">
                <h2>セミナー開催日</h2>
            <!----------------- seminar ------------------->
            <ul class="schedule-list">
                                        <li>
                                            <p class="date">
                                                8月19日(水) 10:00~10:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月19日(水) 15:00~15:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月21日(金) 10:00~10:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月21日(金) 15:00~15:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月24日(月) 10:00~10:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月24日(月) 15:00~15:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月26日(水) 10:00~10:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月26日(水) 15:00~15:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月28日(金) 10:00~10:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月28日(金) 15:00~15:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月31日(月) 10:00~10:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                                        <li>
                                            <p class="date">
                                                8月31日(月) 15:00~15:45
                                            </p>
                                            <a class="btn" href="#form-bottom">申し込む</a>
                                        </li>
                </ul>
            </section>
            <section id="form-bottom" class="section-form">
                <h2>お申込みはコチラ</h2>
                <form action="" method="post" class="form">
                    <?php if( !empty($error) ): ?>
                        <ul class="error-list">
                        <?php foreach( $error as $value ): ?>
                            <li><?php echo $value; ?></li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <!-- 氏名記入 -->
                    <div class="form-heading">
                        <label class="required">氏名</label>
                        <div class="answer-box">
                            <div class="name-box">
                                <input class="form-control name1 is_input" type="text" id="name" name="last_name" placeholder="姓" value="<?php if( !empty($clean['last_name']) ) { echo $clean['last_name']; } ?>" required>
                                <input class="form-control is_input" type="text" id="name" name="first_name" placeholder="名" value="<?php if( !empty($clean['first_name']) ) { echo $clean['first_name']; } ?>" required>
                            </div>
                        </div>
                    </div>    
                    <!-- 会社名記入 -->
                    <div class="form-heading">
                        <label class="required">会社名</label>
                        <div class="answer-box">
                            <input class="form-control is_input" type="text" id="company-name" name="user_company" placeholder="例：株式会社○○" value="<?php if( !empty($clean['user_company']) ) { echo $clean['user_company']; } ?>" required>
                        </div>
                    </div>
                    <!-- 資本金記入 -->
                    <div class="form-heading">
                        <label class="required">資本金</label>
                        <div class="answer-box">
                            <select name="company_capital" id="company-capital" required>
                            <?php if( !empty($company_capitals) ): ?>
                                <?php foreach( $company_capitals as $company_capital ): ?>
                                    <option value="<?php echo $company_capital; ?>" <?php if( !empty($clean['company_capital']) && $clean['company_capital'] === $company_capital ){ echo 'selected'; } ?>><?php echo $company_capital; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <!--従業員数記入 -->
                    <div class="form-heading">
                        <label class="required">従業員数</label>
                        <div class="answer-box">
                            <input class="form-control" type="text" id="number-of-employees" class="is_input" name="number_of_employees" placeholder="例：100" value="<?php if( !empty($clean['number_of_employees']) ) { echo $clean['number_of_employees']; } ?>" required>
                        </div>
                    </div>
                    <!-- メールアドレス記入 -->
                    <div class="form-heading">
                        <label class="required" for="mail">メールアドレス</label>
                        <div class="answer-box">
                            <input class="form-control is_input" type="text" id="mail" name="user_mail" placeholder="例：example@officestation.jp"  value="<?php if( !empty($clean['user_mail']) ) { echo $clean['user_mail']; } ?>" required>
                        </div>
                    </div>
                    <!-- 電話番号記入 -->
                    <div class="form-heading">
                        <label class="required" for="tel">電話番号</label>
                        <div class="answer-box">
                            <input class="form-control" type="text" id="tel" name="user_tel"  placeholder="例：000-000-0000"  value="<?php if( !empty($clean['user_tel']) ) { echo $clean['user_tel']; } ?>" required>
                        </div>
                    </div>
                    <!-- 参加予定のセミナー記入 -->
                    <div class="form-heading">
                        <label class="required">参加予定のセミナー</label>
                        <div class="answer-box">
                            <select name="seminar" required>
                                <option value=""></option>
                                <?php if( !empty($seminars) ): ?>
                                    <?php foreach( $seminars as $seminar ): ?>
                                        <option value="<?php echo $seminar; ?>" <?php if( !empty($clean['seminar']) && $clean['seminar'] === $seminar ){ echo 'selected'; } ?>><?php echo $seminar; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <!-- 広告アンケート記入 -->
                    <div class="form-heading inflow_source">
                        <label>「オフィスステーション 年末調整」を知った理由</label>
                        <div class="answer-box">
                            <?php if( !empty($inflow_sources) ): ?>
                                <?php foreach( $inflow_sources as $id => $value ): ?>
                                    <label for="<?php echo $id; ?>"><input id="<?php echo $id; ?>" type="radio" name="inflow_source" value="<?php echo $value; ?>" <?php if( !empty($clean['inflow_source']) && $clean['inflow_source'] === $value ){ echo 'checked'; } ?>><?php echo $value; ?></label>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <input type="text" name="othertext" id="othertext" value="<?php if( !empty($clean['othertext']) ) { echo $clean['othertext']; } ?>">
                        </div>
                    </div>
                    <!-- プライバシー同意 -->
                    <div class="form-heading">
                        <label class="required">プライバシーステートメントを確認し、同意します。</label>
                        <div class="answer-box">
                            <span class="value">
                                <input type="checkbox" name="agree" id="agree" value="同意する" <?php if( $clean['agree'] === "同意する" ){ echo 'checked'; } ?> required>
                                <label class="agree" for="agree">同意する</label>
                            </span>
                            <p class="description"><a href="https://www.officestation.jp/privacy-statement/" target="_blank">プライバシーステートメント</a></p>
                        </div>
                    </div>
                    <!-- 私はロボットではありません -->
                    <div class="g-recaptcha" data-callback="clearcall" data-sitekey="<?php echo $site_key ?>"></div>
                    <!-- 確認画面へボタン -->
                    <p class="submit text-center">
                        <input id="os-submit" type="submit" name="btn_confirm" class="button" value="確認画面へ" disabled>
                    </p>
                </form>
            </section>
        <?php else: ?>
            <section id="form-bottom" class="section-form">
                <h2 class="mt40">お申込み内容のご確認</h2>
                <form action="" method="post" class="form">
                    <?php if( !empty($error) ): ?>
                        <ul class="error-list">
                        <?php foreach( $error as $value ): ?>
                            <li><?php echo $value; ?></li>
                        <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <!-- 氏名記入 -->
                    <div class="form-heading">
                        <label class="required">氏名</label>
                        <div class="answer-box">
                            <div class="name-box">
                                <input class="form-control name1 is_input" type="text" id="name" name="last_name" placeholder="姓" value="<?php if( !empty($clean['last_name']) ) { echo $clean['last_name']; } ?>" required>
                                <input class="form-control is_input" type="text" id="name" name="first_name" placeholder="名" value="<?php if( !empty($clean['first_name']) ) { echo $clean['first_name']; } ?>" required>
                            </div>
                        </div>
                    </div>    
                    <!-- 会社名記入 -->
                    <div class="form-heading">
                        <label class="required">会社名</label>
                        <div class="answer-box">
                            <input class="form-control is_input" type="text" id="company-name" name="user_company" placeholder="例：株式会社○○" value="<?php if( !empty($clean['user_company']) ) { echo $clean['user_company']; } ?>" required>
                        </div>
                    </div>
                    <!-- 資本金記入 -->
                    <div class="form-heading">
                        <label class="required">資本金</label>
                        <div class="answer-box">
                            <select name="company_capital" id="company-capital" required>
                            <?php if( !empty($company_capitals) ): ?>
                                <?php foreach( $company_capitals as $company_capital ): ?>
                                    <option value="<?php echo $company_capital; ?>" <?php if( !empty($clean['company_capital']) && $clean['company_capital'] === $company_capital ){ echo 'selected'; } ?>><?php echo $company_capital; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <!--従業員数記入 -->
                    <div class="form-heading">
                        <label class="required">従業員数</label>
                        <div class="answer-box">
                            <input class="form-control" type="text" id="number-of-employees" class="is_input" name="number_of_employees" placeholder="例：100" value="<?php if( !empty($clean['number_of_employees']) ) { echo $clean['number_of_employees']; } ?>" required>
                        </div>
                    </div>
                    <!-- メールアドレス記入 -->
                    <div class="form-heading">
                        <label class="required" for="mail">メールアドレス</label>
                        <div class="answer-box">
                            <input class="form-control is_input" type="text" id="mail" name="user_mail" placeholder="例：example@officestation.jp"  value="<?php if( !empty($clean['user_mail']) ) { echo $clean['user_mail']; } ?>" required>
                        </div>
                    </div>
                    <!-- 電話番号記入 -->
                    <div class="form-heading">
                        <label class="required" for="tel">電話番号</label>
                        <div class="answer-box">
                            <input class="form-control" type="text" id="tel" name="user_tel"  placeholder="例：000-000-0000"  value="<?php if( !empty($clean['user_tel']) ) { echo $clean['user_tel']; } ?>" required>
                        </div>
                    </div>
                    <!-- 参加予定のセミナー記入 -->
                    <div class="form-heading">
                        <label class="required">参加予定のセミナー</label>
                        <div class="answer-box">
                            <select name="seminar" required>
                                <option value=""></option>
                                <?php if( !empty($seminars) ): ?>
                                    <?php foreach( $seminars as $seminar ): ?>
                                        <option value="<?php echo $seminar; ?>" <?php if( !empty($clean['seminar']) && $clean['seminar'] === $seminar ){ echo 'selected'; } ?>><?php echo $seminar; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </div>
                    <!-- 広告アンケート記入 -->
                    <div class="form-heading inflow_source">
                        <label>「オフィスステーション 年末調整」を知った理由</label>
                        <div class="answer-box">
                            <?php if( !empty($inflow_sources) ): ?>
                                <?php foreach( $inflow_sources as $id => $value ): ?>
                                    <label for="<?php echo $id; ?>"><input id="<?php echo $id; ?>" type="radio" name="inflow_source" value="<?php echo $value; ?>" <?php if( !empty($clean['inflow_source']) && $clean['inflow_source'] === $value ){ echo 'checked'; } ?>><?php echo $value; ?></label>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <input type="text" name="othertext" id="othertext" value="<?php if( !empty($clean['othertext']) ) { echo $clean['othertext']; } ?>">
                        </div>
                    </div>
                    <!-- プライバシー同意 -->
                    <div class="form-heading">
                        <label class="required">プライバシーステートメントを確認し、同意します。</label>
                        <div class="answer-box">
                            <span class="value">
                                <input type="checkbox" name="agree" id="agree" value="同意する" <?php if( $clean['agree'] === "同意する" ){ echo 'checked'; } ?> required>
                                <label class="agree" for="agree">同意する</label>
                            </span>
                            <p class="description"><a href="https://www.officestation.jp/privacy-statement/" target="_blank">プライバシーステートメント</a></p>
                        </div>
                    </div>
                    <!-- 私はロボットではありません -->
                    <div class="g-recaptcha" data-callback="clearcall" data-sitekey="<?php echo $site_key ?>"></div>
                    <!-- 確認画面へボタン -->
                    <p class="submit text-center">
                        <input id="os-submit" type="submit" name="btn_confirm" class="button" value="確認画面へ" disabled>
                    </p>
                </form>
            </section>
        <?php endif; ?>
    <?php elseif( $page_flag === 1 ): ?>
        <section id="form-bottom" class="section-form">
            <h2 class="mt40">お申込み内容のご確認</h2>
            <form action="form-thanks.php" method="post">
                <!-- 氏名表示 -->
                <div class="form-heading">
                    <label class="required">氏名</label>
                    <p><?php echo $clean['last_name']. " ". $clean['first_name']; ?></p>
                </div>    
                <!-- 会社名表示 -->
                <div class="form-heading">
                    <label class="required">会社名</label>
                    <p><?php echo $clean['user_company']; ?></p>
                </div>
                <!-- 資本金表示 -->
                <div class="form-heading">
                    <label class="required">資本金</label>
                    <p><?php echo $clean['company_capital']; ?></p>
                </div>
                <!-- 従業員数表示 -->
                <div class="form-heading">
                    <label class="required">従業員数</label>
                    <p><?php echo $clean['number_of_employees']; ?></p>
                </div>
                <!-- メールアドレス表示 -->
                <div class="form-heading">
                    <label class="required" for="mail">メールアドレス</label>
                    <p><?php echo $clean['user_mail']; ?></p>
                </div>
                <!-- 電話番号表示 -->
                <div class="form-heading">
                    <label class="required" for="tel">電話番号</label>
                    <p><?php echo $clean['user_tel']; ?></p>
                </div>
                <!-- 参加予定のセミナー表示 -->
                <div class="form-heading">
                    <label class="required">参加予定のセミナー</label>
                    <p><?php echo $clean['seminar']; ?></p>
                </div>
                <!-- 広告アンケート表示 -->
                <div class="form-heading">
                <label class="" for="inflow_source">「オフィスステーション 年末調整」を知った理由</label>
                <p>
                    <?php
                        if( ($clean['inflow_source'] === "その他") && (!empty($clean['othertext'])) ) {
                            $inflow_source = "その他:" .$clean['othertext'];
                            $clean['inflow_source'] = $inflow_source;
                        }
                        echo $clean['inflow_source'];
                    ?>
                </div>
                <!-- プライバシー同意 -->
                <div class="form-heading">
                    <label class="required" for="">プライバシーステートメントを確認し、同意します。</label>
                    <p><?php echo $clean['agree']; ?></p>
                </div>
                <!-- ボタン -->
                <div class="btns">
                    <input class="button" type="button" name="btn_back" value="戻る" onclick="history.back()">
                    <input id="os-submit" class="button" type="submit" name="btn_submit" value="送信する">
                </div>
                <!-- hiddenでサンクス画面へデータ引き渡し -->
                <input type="hidden" name="last_name" value="<?php echo $clean['last_name']; ?>">
                <input type="hidden" name="first_name" value="<?php echo $clean['first_name']; ?>">
                <input type="hidden" name="user_company" value="<?php echo $clean['user_company']; ?>">
                <input type="hidden" name="company_capital" value="<?php echo $clean['company_capital']; ?>">
                <input type="hidden" name="number_of_employees" value="<?php echo $clean['number_of_employees']; ?>">
                <input type="hidden" name="user_mail" value="<?php echo $clean['user_mail']; ?>">
                <input type="hidden" name="user_tel" value="<?php echo $clean['user_tel']; ?>">
                <input type="hidden" name="seminar" value="<?php echo $clean['seminar']; ?>">
                <input type="hidden" name="inflow_source" value="<?php echo $clean['inflow_source']; ?>">
                <input type="hidden" name="agree" value="<?php echo $clean['agree']; ?>">
            </form>
        </section>
    <?php endif; ?>
    <!--content-->
    <footer id="footer-lp-nencho-seminar">
        <div class="foooter-nolink-area">
            <div class="footer-logo"><img src="/img/common/c_logo.png" alt="株式会社エフアンドエム"></div>
            <div class="right">
                <div class="footer-copy">
                    <p>&copy; 2015 F&M co.,ltd.</p>
                </div>
            </div>
        </div>
    </footer>
    <script src="/module/form/recaptcha.js"></script>
    <script>
        $(function(){
            $('a[href^="#"]').click(function(){
            var speed = 500;
            var href= $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            //ヘッダーの高さを取得
            var header = $('header').height();
            //ヘッダーの高さを引く
            var position = target.offset().top - header;
            $("html, body").animate({scrollTop:position}, speed, "swing");
            return false;
            });
        });
    </script>
</body>
</html>
