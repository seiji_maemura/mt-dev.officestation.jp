<?php
    include '../../../module/form/main.php';
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <meta name="robots" content="noindex,nofollow">
    <title>年末調整セミナーお申込みありがとうございます。</title>
    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>

    <link rel="canonical" href="https://www.officestation.jp/seminar/form-nencho/form-thanks.php" />
    <meta property="og:title" content="年末調整セミナーお申込みありがとうございます。" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.officestation.jp/seminar/form-nencho/form.php" />
    <meta property="og:image" content="https://www.officestation.jp/img/ogimage.jpg" />
    <meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
    <meta property="og:image:secure_url" content="https://www.officestation.jp/img/ogimage.jpg" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="年末調整セミナーお申込みありがとうございます。" />
    <meta name="twitter:image" content="https://www.officestation.jp/img/ogimage.jpg" />

    <link rel='dns-prefetch' href='//s.w.org' />
    <!-- Markup (JSON-LD) structured in schema.org ver.4.6.5 START -->
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [
            {
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "https://www.officestation.jp",
                    "name": "オフィスステーション"
                }
            },
            {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "https://www.officestation.jp/seminar/",
                    "name": "セミナー一覧"
                }
            }
            {
                "@type": "ListItem",
                "position": 3,
                "item": {
                    "@id": "https://www.officestation.jp/seminar/form-nencho/form.php",
                    "name": "年末調整セミナーお申し込み"
                }
            }
        ]
    }
    </script>
    <!-- Markup (JSON-LD) structured in schema.org END -->

    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/styles_sp.css">
    <link rel="stylesheet" href="/css/styles_.css">
    <link rel="stylesheet" href="/css/styles-t.css">
    <link rel="stylesheet" href="/css/styles_sp-t.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/common_sp.css">
    <link rel="stylesheet" href="/css/styles_i.css">

    <!-- JS Setting-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery.matchHeight.js"></script>
    <script src="/js/scripts.js"></script>

    <!-- Google Analyticsへのデータ連携 -->
    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({'customer': '2', 'employees': '<?php if( $clean['number_of_employees'] != "" ) { echo $clean['number_of_employees']; } ?>', 'capital': '<?php if( !empty($clean['company_capital']) ) { echo $clean['company_capital']; } ?>', 'ads': '<?php if( !empty($clean['inflow_source']) ) { echo $clean['inflow_source']; } ?>'});
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
    <!-- End Google Tag Manager -->

</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="frame-outer">
        <header>
            <div id="header" class="header">
                <div class="header-inner">
                    <div class="header-logo">
                        <a href="/">
                            <div class="img" id="header-logo">
                            <div class="logo02"></div>
                            </div>
                        </a>
                    </div>
                    <nav class="header-nav">
                        <ul>
                            <li class="subpage pc parentWrap">
                                <div>
                                    <span class="parent">9つの神対応</span>
                                    <div class="childWrap">
                                        <div class="childWrap-inner cf">
                                            <div class="areaTit" data-mh="navi-child">
                                                <p class="tit"><a href="/feature/">9つの神対応</a></p>
                                            </div>
                                            <ul class="child s2" data-mh="navi-child">
                                                <li itemprop="name" class="childItem"><a href="/feature/chohyou/" itemprop="URL"><span class="num">1</span>対応帳票・イベント</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/cost/"><span class="num">2</span>低コスト</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/support/"><span class="num">3</span>サポート体制</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/security/"><span class="num">4</span>セキュリティ</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/egovapi/"><span class="num">5</span>e-Gov対応</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/efficiency/"><span class="num">6</span>業務効率化</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/automatic/"><span class="num">7</span>法改正自動対応</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/cooperation/"><span class="num">8</span>他社システム連携</a></li>
                                                <li itemprop="name" class="childItem"><a href="/feature/alacarte/"><span class="num">9</span>アラカルト利用対応</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="subpage pc"><a href="/about/"><span>オフィスステーションとは?</span></a></li>
                            <li class="subpage pc parentWrap">
                                <div>
                                    <span class="parent">製品</span>
                                    <div class="childWrap">
                                        <div class="childWrap-inner cf">
                                            <div class="areaTit" data-mh="navi-child">
                                                <p class="tit">製品</p>
                                            </div>
                                            <ul class="child s3" data-mh="navi-child">
                                                <li itemprop="name" class="childItem"><a href="/products/">製品一覧</a></li>
                                                <li itemprop="name" class="childItem"><a href="/roumu/" target="_blank">オフィスステーション 労務</a></li>
                                                <li itemprop="name" class="childItem"><a href="/nencho/" target="_blank">オフィスステーション 年末調整</a></li>
                                                <li itemprop="name" class="childItem"><a href="/kyuyomeisai/" target="_blank">オフィスステーション Web給与明細</a></li>
                                                <li itemprop="name" class="childItem"><a href="/yukyu/" target="_blank">オフィスステーション 有休管理</a></li>
                                                <li itemprop="name" class="childItem"><a href="/mynumber-company/" target="_blank">オフィスステーション マイナンバー</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="subpage pc"><a href="/function/"><span>機能</span></a></li>
                            <li class="subpage pc"><a href="/pricing/"><span>料金</span></a></li>
                            <li class="subpage pc"><a href="/case/"><span>導入事例</span></a></li>
                            <li class="menu" id="header-menu"><div class="img02"></div></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div id="header-f"><div class="header-f-in">
                <div class="menu" id="header-menu-f"><img src="/img/common/img_menu01.png" alt="MENU"></div>
            </div></div>
        </header>

        <div id="g-nav-r">
            <div class="g-nav-in">
                <div class="g-nav-content">
                    <div class="g-nav-close">
                        <div id="g-nav-close">×</div>
                        <div class="txt">SOCIAL MEDIA</div>
                        <div class="sns"><ul>
                            <li><a href="https://www.facebook.com/officestation.series/" target="_blank" class="facebook"><img src="/img/common/logo_facebook.jpg" alt="facebook"></a></li>
                            <li><a href="https://twitter.com/officestation_s" target="_blank" class="twitter"><img src="/img/common/logo_twitter.jpg" alt="twitter"></a></li>
                            <li><a href="https://www.instagram.com/officestation_sppt/" target="_blank" class="instagram"><img src="/img/common/logo_instagram.jpg" alt="instagram"></a></li>
                        </ul></div>
                    </div>
                    <div class="g-nav-logo"><a href="/"><img src="/img/lp/logo05.png" alt="オフィスステーション"></a></div>
                    <div class="g-nav-content-inner">
                            <div class="list">
                                <ul>
                                    <li id="gl1"><a class="js-toggle-tit">9つの神対応</a>
                                        <div class="g-nav-subcontent">
                                            <div class="list">
                                                <ul>
                                                    <li><a href="/feature/">9つの神対応一覧</a></li>
                                                    <li><a href="/feature/chohyou/"><span class="num">1.</span>対応帳票・イベント</a></li>
                                                    <li><a href="/feature/cost/"><span class="num">2.</span>低コスト</a></li>
                                                    <li><a href="/feature/support/"><span class="num">3.</span>サポート体制</a></li>
                                                    <li><a href="/feature/security/"><span class="num">4.</span>セキュリティ</a></li>
                                                    <li><a href="/feature/egovapi/"><span class="num">5.</span>e-Gov対応</a></li>
                                                    <li><a href="/feature/efficiency/"><span class="num">6.</span>業務効率化</a></li>
                                                    <li><a href="/feature/automatic/"><span class="num">7.</span>法改正自動対応</a></li>
                                                    <li><a href="/feature/cooperation/"><span class="num">8.</span>他社システム連携</a></li>
                                                    <li><a href="/feature/alacarte/"><span class="num">9.</span>アラカルト利用対応</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="gl2"><a href="/about/">オフィスステーションとは？</a></li>
                                    <li id="gl3"><a class="js-toggle-tit">製品</a>
                                        <div class="g-nav-subcontent">
                                            <div class="list">
                                                <ul>
                                                    <li><a href="/products/">製品一覧</a></li>
                                                    <li><a href="/roumu/">オフィスステーション 労務</a></li>
                                                    <li><a href="/nencho/">オフィスステーション 年末調整</a></li>
                                                    <li><a href="/kyuyomeisai/">オフィスステーション Web給与明細</a></li>
                                                    <li><a href="/yukyu/">オフィスステーション 有休管理</a></li>
                                                    <li><a href="/mynumber-company/">オフィスステーション マイナンバー</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li id="gl4"><a href="/function/">機能</a></li>
                                    <li id="gl5"><a href="/pricing/">料金</a></li>
                                    <li id="gl3"><a href="/case/">導入事例</a></li>
                                    <li id="gl9"><a href="/helpcenter/" target="_blank">ヘルプセンター</a></li>
                                    <li id="gl6"><a href="/yesaq/">Yes!AQ</a></li>
                                    <li id="gl7"><a href="/movie/">メディア・CM</a></li>
                                    <li id="gl8"><a href="/corporate/">会社概要</a></li>
                                </ul>
                            </div>
                            <div class="areaSublist">
                                <ul class="sublist">
                                    <li class="item"><a href="/corporate/infosecure/">情報セキュリティ基本方針</a></li>
                                    <li class="item"><a href="/privacy-statement/">プライバシーステートメント</a></li>
                                </ul>
                                <ul class="sublist">
                                    <li class="item"><a href="/sla/">SLA</a></li>
                                </ul>

                                <ul class="sns sp cf">
                                    <li class="item"><a href="https://www.facebook.com/officestation.series/" target="_blank" class="facebook"></a></li>
                                    <li class="item"><a href="https://twitter.com/officestation_s" target="_blank" class="twitter"></a></li>
                                    <li class="item"><a href="https://www.instagram.com/officestation_sppt/" target="_blank" class="instagram"></a></li>
                                </ul>
                            </div>
                            <div class="tel">
                                <div class="num">
                                    <div><span data-action="call" data-tel="0362253127">03-6225-3127</span></div>
                                    <div><span data-action="call" data-tel="0663397205">06-6339-7205</span></div>
                                </div>
                                <p class="time">電話受付時間　平日9:30～12:00 / 13:00～17:00</p>
                            </div>
                    </div>
                </div>
            </div>
            <div class="f-footer"><ul>
                <li class="f1"><a href="/">
                    <span class="logo"><img src="/img/common/logo02.png" alt="オフィスステーション"></span>
                </a></li>
                <li class="f2"><a href="https://service.officestation.jp/guest/web-application/trial" target="_blank">
                    <span class="ico"><img src="/img/lp/ico_f02.png" alt=""></span>
                    <span class="txt">30日間無料お試し</span>
                    </a></li>
                <li class="f3"><a href="/document/">
                    <span class="ico"><img src="/img/lp/ico_f03.png" alt=""></span>
                    <span class="txt">資料ダウンロード</span>
                </a></li>
                <li class="f4"><a href="/pricing/">
                    <span class="ico"><img src="/img/lp/ico_f01.png" alt=""></span>
                    <span class="txt">料金案内</span>
                </a></li>
                <li class="f5"><a href="/form/">
                    <span class="ico"><img src="/img/lp/ico_f05.png" alt=""></span>
                    <span class="txt">お問い合わせ</span>
                </a></li>
            </ul></div>
        </div>

        <div class="content">
            <div class="page-ttl">
                <h1>
                    <span>セミナーお申込み</span>
                </h1>
            </div>
            <div id="bread">
                <ul>
                    <li><a href="/">HOME</a></li>
                    <li><a href="/seminar/">セミナー一覧</a></li>
                    <li><a href="/seminar/form.php">セミナーお申込み</a></li>
                </ul>
            </div>
            <section class="block">
                <div class="section-inner">
                    <div class="wrap">
                        <div class="c-ttl2 m-t30 m-b80 sp_m-b40">
                            <div class="main">
                                <h2>セミナーお申込みを承りました。</h2>
                            </div>
                        </div>
                        <div class="c-txt m-b40">
                            <p><span class="dis-ib t-l">このたびは、セミナーにお申込みいただき、誠にありがとうございます。<br><br>
                            差出人「ウェビナー情報」、件名「ウェビナー参加申込完了」のメールにてご連絡いたしますので、今しばらくお待ちください。<br>
                            ウェビナー参加時は、そのメール本文のリンク先よりご参加ください。<br><br>
                            今後とも「オフィスステーション」をよろしくお願い申し上げます。</span><p>
                        </div>
                        <!-- Pardotへのデータ連携 -->
                        <?php if( !empty($_POST) && !empty($clean) ) {
                            $last_name = urlencode($clean['last_name']); 
                            $first_name = urlencode($clean['first_name']); 
                            $user_company = urlencode($clean['user_company']); 
                            $company_capital = urlencode($clean['company_capital']); 
                            $number_of_employees = urlencode($clean['number_of_employees']); 
                            $user_mail = urlencode($clean['user_mail']); 
                            $user_tel = urlencode($clean['user_tel']); 
                            $seminar = urlencode($clean['seminar']);
                            $inflow_source = urlencode($clean['inflow_source']); 
                            $agree = urlencode($clean['agree']);

                            echo '<iframe id="iframe" src="https://go.officestation.jp/l/723363/2020-07-07/bqrzn?last_name='.$last_name.'&first_name='.$first_name.'&user_company='.$user_company.'&company_capital='.$company_capital.'&number_of_employees='.$number_of_employees.'&user_mail='.$user_mail.'&user_tel='.$user_tel.'&seminar='.$seminar.'&inflow_source='.$inflow_source.'&agree='.$agree.'" width="1" height="1"></iframe>';
                        }
                        ?>
                    </div>
                </div>
            </section>
        </div>

        <div class="block cv b-c5">
            <div class="block-inner">
                <div class="wrap">
                    <div class="boxs cf">
                        <div class="box-l">
                            <div class="c-ttl">
                                <div class="en">FREE TRIAL</div>
                                <h2 class="jp f-c5 ">30⽇間無料お試し</h2>
                            </div>
                            <p class="c-txt lead">⾃分の⽬で⾒て、⾃分の⼿で触って判断してください。<br>数多くの企業が感じている「価値」を体験してください。</p>
                            <div class="areaList">
                                <ul class="list">
                                    <li class="item">
                                        <span class="num">1</span>
                                        <h3 class="ttl">実データの入力可能</h3>
                                        <p class="txt pc">本番と同じセキュリティ、運用環境を提供するので実際の従業員データ、給与データの入力が可能。電子申請も可能です。※電子申請には電子証明書が必要です。</p>
                                    </li>
                                    <li class="item">
                                        <span class="num">2</span>
                                        <h3 class="ttl">サポートデスク利用可能</h3>
                                        <p class="txt pc">サポートスタッフと必ずお話しいただける環境をお約束。<br>実務経験者とシステム開発者が連携し、迅速な対応を実現します。</p>
                                    </li>
                                    <li class="item">
                                        <span class="num">3</span>
                                        <h3 class="ttl">契約後もそのまま利用可能</h3>
                                        <p class="txt pc">本契約したいときは無料お試しで利用したシステム環境をそのままご利用いただけます。入力データもそのまま本番環境で利用可能です。</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="btn c-btn"><a href="https://service.officestation.jp/guest/web-application/trial" class="c5" target="_blank"><span>30日間無料お試しはこちらから！</span></a></div>
                            <p class="note">※ご安心ください。<br class="sp">自動的に本契約になることはありません。</p>
                        </div>
                        <div class="box-r">
                            <div class="download">
                                <div class="c-ttl">
                                    <h2 class="jp">資料ダウンロード</h2>
                                    <div class="en border-t">DOWNLOAD DOCUMENTS</div>
                                </div>
                                <p class="txt">機能、費用、使い勝手の確認や他社システムとの比較など、<br class="pc">システム選定に必要な資料を準備しています。<br class="pc">社内で検討、稟議申請するための資料としてご活用ください。</p>
                                <div class="c-btn btn"><a href="/document/" class="c6"><span>資料ダウンロードフォーム</span></a></div>
                            </div>
                            <div class="tel">
                                <div class="tit">お電話でのお問い合わせ</div>
                                <div class="txt">資料請求・お見積り、導入方法についてのお問い合わせは、<br class="pc">お電話でも承っております。</div>
                                <div class="num"><span class="sp_m-b10" data-action="call" data-tel="0362253127">03-6225-3127</span><span data-action="call" data-tel="0663397205">06-6339-7205</span></div>
                                <div class="c-txt time">電話受付時間　平日9:30～12:00 / 13:00～17:00</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="footer">
                <div class="wrap">
                    <div class="footer-inner">
                        <div class="footer-t">
                            <div class="footer-l">
                                <p class="tit">社会保険労務士・税理士の方はこちら</p>
                            </div>
                            <div class="footer-r">
                                <p class="txt">士業事務所に究極の業務効率と新たなビジネスの可能性を</p>
                                <div class="link"><ul class="list">
                                    <li class="item"><a href="/pro/" target="_blank">オフィスステーションPro</a></li>
                                    <li class="item"><a href="/pro/partner-shigyo/" target="_blank">オフィスステーション公認パートナー制度</a></li>
                                </ul></div>
                            </div>
                        </div>
                        <div class="footer-b">
                            <div class="footer-l">
                                <div class="footer-logo"><a href="https://www.fmltd.co.jp" target="_blank"><img src="/img/common/c_logo.png" alt="株式会社エフアンドエム"></a></div>
                                <p class="txt">年末調整・労務手続きが劇的に変わる<br>クラウド型 労務・人事管理システム「オフィスステーション」</p>

                                <div class="box">
                                    <p class="search"><a href="https://romsearch.officestation.jp/" target="_blank">労務SEARCH</a></p>
                                    <p class="logos"><img src="/img/common/img_footer_logos.png" alt=""></p>
                                    <p class="is">IS 699609 / ISO 27001 / ISO 27018<br>株式会社エフアンドエムオフィスステーション事業本部</p>
                                </div>

                                <div class="footer-nav pc tab"><ul>
                                    <li><a href="https://www.fmltd.co.jp/privacypolicy.html" target="_blank">個人情報保護方針</a></li>
                                </ul></div>
                            </div>
                            <div class="footer-r">
                                <div class="footer-nav"><ul class="list">
                                    <li class="item"><a href="/about/">オフィスステーションとは？</a></li>
                                    <li class="item"><a href="/feature/">9つの神対応</a></li>
                                    <li class="item"><a href="/case/">導入事例</a></li>
                                    <li class="item"><a href="/products/">機能・製品</a></li>
                                    <li class="item"><a href="/pricing/">料金プラン</a></li>
                                    <li class="item"><a href="/yesaq/">Yes!AQ</a></li>
                                    <li class="item"><a href="/corporate/">会社概要</a></li>
                                    <li class="item"><a href="/form/">お問い合わせ</a></li>
                                    <li class="item"><a href="/helpcenter/" target="_blank">ヘルプセンター</a></li>
                                </ul></div>
                                <div class="cf">
                                    <div class="footer-nav-sub"><ul class="list">
                                        <li class="item"><a href="/corporate/infosecure/">情報セキュリティ基本方針</a></li>
                                        <li class="item"><a href="/sla/">SLA</a></li>
                                        <li class="item"><a href="/privacy-statement/">プライバシーステートメント</a></li>
                                                                    </ul></div>
                                    <div class="footer-sns-copy">
                                        <div class="footer-sns"><ul class="list">
                                            <li class="item"><a href="https://www.facebook.com/officestation.series/" target="_blank"><img src="/img/common/ico_facebook.svg" alt="facebook"></a></li>
                                            <li class="item"><a href="https://twitter.com/officestation_s" target="_blank"><img src="/img/common/ico_twitter.svg" alt="twitter"></a></li>
                                            <li class="item"><a href="https://www.instagram.com/officestation_sppt/" target="_blank"><img src="/img/common/ico_insta.svg" alt="instagram"></a></li>
                                        </ul></div><br>
                                        <div class="footer-copy"><p>&copy; 2015 F&M co.,ltd.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="f-footer" id="f-footer">
            <ul>
                <li class="f1"><a href="/">
                    <span class="logo"><img src="/img/common/logo02.png" alt="オフィスステーション"></span>
                </a></li>
                <li class="f2"><a href="https://service.officestation.jp/guest/web-application/trial" target="_blank">
                    <span class="ico"><img src="/img/lp/ico_f02.png" alt=""></span>
                    <span class="txt">30日間無料お試し</span>
                    </a></li>
                <li class="f3"><a href="/document/">
                    <span class="ico"><img src="/img/lp/ico_f03.png" alt=""></span>
                    <span class="txt">資料ダウンロード</span>
                </a></li>
                <li class="f4"><a href="/pricing/">
                    <span class="ico"><img src="/img/lp/ico_f01.png" alt=""></span>
                    <span class="txt">料金案内</span>
                </a></li>
                <li class="f5"><a href="/form/">
                    <span class="ico"><img src="/img/lp/ico_f05.png" alt=""></span>
                    <span class="txt">お問い合わせ</span>
                </a></li>
            </ul>
        </div>

        <div class="btn-list2">
            <ul class="cf">
                <li class="lib"><a href="/document/"><span class="lib">資料ダウンロード</span></a></li>
                <li class="try"><a href="https://service.officestation.jp/guest/web-application/trial"><span class="try">30日間無料お試し</span></a></li>
                <li class="inq"><a href="/form/"><span class="inq"></span></a></li>
            </ul>
        </div>
    </div>

</body>
</html>
