$(function(){
    var ua = navigator.userAgent;
    if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
        $('head').prepend('<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">');
    } else {
        $('head').prepend('<meta name="viewport" content="width=1024">');
    }
});

$(function() {
     $('a[href^=#].scroll').click(function() {
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $('body,html').animate({scrollTop:position-0}, speed, 'swing');
        return false;
     });
});
$(function() {
  $(window).scroll(function () {
    var s = $(this).scrollTop();
    var m = 200;
    if(s > m) {
      $("#pagetop").fadeIn('slow');
    } else if(s < m) {
      $("#pagetop").fadeOut('slow');
    }
  });


  $(window).bind("scroll", function() {
    var w = $(window).width();
    var x = 767;
    if (x < w) {
        scrollHeight = $(document).height();
        scrollPosition = $(window).height() + $(window).scrollTop();
        footHeight = 40;
        if ( scrollHeight - scrollPosition  <= footHeight ) {
          $("#pagetop").css({"position":"absolute","bottom": "60px"});
        } else {
          $("#pagetop").css({"position":"fixed","bottom": "20px"});
        }
    }
  });

});

$(function() {
  $('.main-img .t2.slide').slick({
    autoplay: true,
    slidesToShow: 3,
    arrows: false,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });
});