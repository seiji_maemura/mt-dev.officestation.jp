<!DOCTYPE html><html lang="ja"><head><meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
<meta name="format-detection" content="telephone=no">
<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
<title>年末調整をかんたん便利に！オフィスステーション 年末調整</title>
<link rel="canonical" href="https://www.officestation.jp/nencho/lp/a02/" />
<meta name="description"  content="" />
<meta property="og:title" content="年末調整をかんたん便利に！オフィスステーション 年末調整" />
<meta property="og:type" content="article" />
<meta property="og:url" content="" />
<meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
<meta property="og:image" content="" />
<meta property="og:image:secure_url" content="" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="年末調整をかんたん便利に！オフィスステーション 年末調整" />
<meta name="twitter:image" content="" />
<link rel="stylesheet" href="./css/common.css">
<link rel="stylesheet" href="./css/common_sp.css">
<link rel="stylesheet" href="./css/styles.css">
<link rel="stylesheet" href="./css/styles_sp.css">
<link rel="stylesheet" href="./css/slick.css">
<link rel="stylesheet" href="./css/slick-theme.css">
<link rel="stylesheet" href="./css/styles_i.css">
<script src="/js/jquery.js"></script>
<script src="./js/slick.min.js"></script>
<script src="./js/scripts.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
<!-- End Google Tag Manager -->
</head>
<body id="top" class="">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div id="frame-outer">
<header><div id="header" class="header">
    <div class="header-inner">
        <div class="logo"><h1><img src="./img/logo.png" alt="オフィスステーション 年末調整"></h1></div>
    </div>
</div></header>
<main id="main">
<div class="main-img"><div class="wrap">
    <div class="txt-area">
        <div class="t1"><h2>
            <span class="t1-1">年末調整を効率化</span>
            <span class="t1-2">ペーパーレス化で工数1/3に</span>
        </h2></div>
        <ul class="t2 slide">
            <li><div><div class="f-s60">導入実績</div>
                    <div>8,000社突破</div>
                </div><div class="sub">オフィスステーションシリーズ累計</div>
            </li>
            <li><div><div class="f-s60">今の給与システムは</div>
                    <div>そのままでOK !</div>
                </div>
            </li>
            <li><div><div class="f-s60">1人あたりたった</div>
                    <div><span class="bg">年間</span>400円<span class="f-s50">(税抜)</span></div>
                </div>
            </li>
        </ul>
        <div class="tag pc"><img src="./img/tag.png" alt="製造業向け労務クラウド 売り上げ No.1"></div>
        <div class="tag sp"><img src="./img/tag_sp.png" alt="製造業向け労務クラウド 売り上げ No.1"></div>
    </div>
    <div class="img-area"><img src="./img/main_pc_sp.png" alt="オフィスステーション 年末調整"></div>
    <div class="txt-area2">
        <p>※ミック経済研究所「HRTechクラウド市場の<br class="pc">実態と展望2019年度版」より</p>
    </div>
</div></div>
<div class="wrap">
    <div class="c-btn">
        <a href="https://service.officestation.jp/guest/web-application/trial" target="_blank" class=""><span class="">30日間無料お試し</span></a>
        <a href="/document/" class="c2"><span class="">資料ダウンロード</span></a>
    </div>
</div>
<section class="block border-b2" id="">
<div class="section-inner p-b0"><div class="wrap">
    <div class="time-wrap3">
        <div class="block">
            <div class="ttl">年末調整の時期だけ遅くまで残業が必要</div>
            <p>
                １年間の給与業務の集大成とも言える年末調整。普段の業務では残業が少ないのに、年末調整の時期に限って遅くまで残業が必要な方。また、夜遅くなると集中力もなくなり、計算ミスにつながってしまうのが悩みという方。
            </p>
        </div>
        <div class="block">
            <div class="ttl">年末調整書類の未回収者の把握と回収が大変</div>
            <p>
                従業員数が多く、年末調整書類の回収も大変で未回収者には個別に電話やメールで追いかけしなくてはならないので大変！という方。
            </p>
        </div>
    </div>
    <div class="time-wrap">
        <div class="time-t1">
            <div class="time-t1-3">年末調整にかかる時間を‥</div>
            <div class="time-t1-1"><span>100</span>時間</div>
            <div class="time-t1-2">も削減</div>
            <div class="img pc"><img src="/img/lp/d_arrow.svg"></div>
        </div>
        <div class="time-block">
            <div class="time-box">
                <div class="time-box-in">
                    <div class="o-t">
                        <div class="t1">導入前</div>
                        <div class="t2"><span>152</span>時間</div>
                    </div>
                    <div class="txt-area">
                        <div class="ttl">導入前の作業</div>
                        <div class="txt">紙収集では、未提出者への追いかけや、内容の不備チェックなどでかなり時間がかかっていた。</div>
                    </div>
                </div>
                <div class="img-area"><img src="./img/time_img01.png" alt="導入前"></div>
            </div>
            <div class="time-t1 sp">
                <div class="img"><img src="/img/lp/d_arrow.svg"></div>
            </div>
            <div class="time-box s2">
                <div class="time-box-in">
                    <div class="o-t">
                        <div class="t1">導入後</div>
                        <div class="t2"><span>52</span>時間</div>
                    </div>
                    <div class="txt-area">
                        <div class="ttl">導入後の作業</div>
                        <div class="txt">従業員への一括メール送信でタスク通知をおこなえるほか、前年度の入力内容差異も一目瞭然で確認できます。</div>
                    </div>
                </div>
                <div class="img-area"><img src="./img/time_img02.png" alt="導入後"></div>
            </div>
        </div>
    </div>
    <div class="time-wrap2">
        <div class="box">
            <p class="note01 sp">※左右にスワイプすることで表の全体が見れます。</p>
            <div id="table-scroll" class="table data-table-l s-over">
                <table class="b1 boder-c2">
                    <tr><th rowspan="2" class="b1 f-c-wh" width="160">
                            <span>導入<span class="f-s130 f-c2">前</span>の<br>作業の流れ</span>
                        </th>
                        <th class="b-c-gr f-c6 f-normal p1" width="210"><div class="in">
                            <span class="ico01">人事担当者</span>
                            <span class="num">1</span>
                        </div></th>
                        <th class="b-c-gr f-c5 f-normal p1" width="210"><div class="in">
                            <span class="ico02">従業員</span>
                            <span class="num">2</span>
                        </div></th>
                        <th class="b-c-gr f-c6 f-normal p1" width="210"><div class="in">
                            <span class="ico01">人事担当者</span>
                            <span class="num">3</span>
                        </div></th>
                        <th class="b-c-gr f-c6 f-normal p1" width="210"><div class="in">
                            <span class="ico01">人事担当者</span>
                            <span class="num">4</span>
                        </div></th>
                    </tr>
                    <tr>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">申告書のプレ印字した申告書を従業員に配布</td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">申告書に必要事項を手書入力・捺印</td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">内容チェック・不備差戻<br>未提出者の追いかけ</td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">給与システムへのデータ入力・チェック作業</td>
                    </tr>
                    <tr><th rowspan="2" class="b3 f-c-wh">
                            <span>導入<span class="f-s130 f-c6">後</span>の<br>作業の流れ</span>
                        </th>
                        <th class="b-c10 f-c6 f-normal p1"><div class="in">
                            <span class="ico01">人事担当者</span>
                            <span class="num c2">1</span>
                        </div></th>
                        <th class="b-c10 f-c5 f-normal p1"><div class="in">
                            <span class="ico02">従業員</span>
                            <span class="num c2">2</span>
                        </div></th>
                        <th class="b-c10 f-c6 f-normal p1"><div class="in">
                            <span class="ico01">人事担当者</span>
                            <span class="num c2">3</span>
                        </div></th>
                        <th class="b-c10 f-c6 f-normal p1"><div class="in">
                            <span class="ico01">人事担当者</span>
                            <span class="num c2">4</span>
                        </div></th>
                    </tr>
                    <tr>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">年末調整機能で対象従業員への年末調整タスクを追加</td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">パソコン・スマートフォンでログインし、年末調整の質問に回答・確認して申請。添付書類を人事担当者に提出</td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">従業員が入力した内容をシステム内で不備確認。<br>未提出者に絞り込み、一斉に催促メールを送信</td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20">給与システムと連携。<br>他社の申告システムへデータを送信・提出</td>
                    </tr>
                    <tr class="bt2"><th rowspan="2" class="f-c6"><div class="in2">
                            <span class="ico">！</span><span class="f-s120">ここが変わる</span>
                        </div></th>
                        <th class="b-c-gr f-c8 f-bold p2"><div class="in">
                            <div><span class="f-s130">業務</span><br>短縮率</div>
                            <div class="bg-par"><div><span>30</span>%<br>短縮</div></div>
                        </div></th>
                        <th class="b-c-gr f-c4 f-bold p2"><div class="in">
                            <div><span class="f-s130">業務</span><br>短縮率</div>
                            <div class="bg-par c2"><div><span>50</span>%<br>短縮</div></div>
                        </div></th>
                        <th class="b-c-gr f-c7 f-bold p2"><div class="in">
                            <div><span class="f-s130">業務</span><br>短縮率</div>
                            <div class="bg-par c3"><div><span>80</span>%<br>短縮</div></div>
                        </div></th>
                        <th class="b-c-gr f-c7 f-bold p2"><div class="in">
                            <div><span class="f-s130">業務</span><br>短縮率</div>
                            <div class="bg-par c3"><div><span>80</span>%<br>短縮</div></div>
                        </div></th>
                    </tr>
                    <tr>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20"><div class="in">
                            <div>
                                プレ印字→<span class="f-s110 f-bold f-c6">不要</span><br>
                                手渡し→<span class="f-s110 f-bold f-c6">不要</span>
                            </div>
                            <div class="arrow m-t10 m-b10"><img src="/img/lp/d_arrow.svg" alt=""></div>
                            <div class="f-bold f-c4">
                                対象者を画面から選んで<br>申告依頼を一括送信！
                            </div>
                        </div></td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20"><div class="in">
                            <div>
                                手書き、書き直し→<span class="f-s110 f-bold f-c6">不要</span><br>
                                捺印→<span class="f-s110 f-bold f-c6">不要</span>
                            </div>
                            <div class="arrow m-t10 m-b10"><img src="/img/lp/d_arrow.svg" alt=""></div>
                            <div class="f-bold f-c4">
                                画面から入力するだけ！
                            </div>
                        </div></td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20"><div class="in">
                            <div>
                                提出状況管理→<span class="f-s110 f-bold f-c6">不要</span><br>
                                全項目チェック→<span class="f-s110 f-bold f-c6">不要</span><br>
                                未提出者への連絡→<span class="f-s110 f-bold f-c6">不要</span>
                            </div>
                            <div class="arrow m-t10 m-b10"><img src="/img/lp/d_arrow.svg" alt=""></div>
                            <div class="f-bold f-c4">
                                提出状況は一覧表示<br>基本的な不備は自動チェック連絡は一斉送信
                            </div>
                        </div></td>
                        <td class="t-l v-t f-c-bk f-s90 f-lighter p-b20"><div class="in">
                            <div>
                                紙からのデータ入力→<span class="f-s110 f-bold f-c6">不要</span><br>
                                データチェック→<span class="f-s110 f-bold f-c6">不要</span>
                            </div>
                            <div class="arrow m-t10 m-b10"><img src="/img/lp/d_arrow.svg" alt=""></div>
                            <div class="f-bold f-c4">
                                ボタン１つで自動連携
                            </div>
                        </div></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div></div>
</section>
<section class="block " id="">
<div class="section-inner p-b0" id="features01"><div class="wrap">
    <div class="c-ttl"><div class="c-ttl-in">
        <h3>年末調整ソフトの特徴</h3>
    </div></div>
    <div class="block-ttl">
        <div class="num">1</div>
        <div class="ttl">
            <div class="sub">面倒だった年末調整がかんたん・便利に！</div>
            <div class="main">従業員もらくらく！2ステップで年末調整</div>
        </div>
    </div>
    <div class="points-wrap">
        <div class="points-block"><div class="points-block-inner2">
            <div class="step">STEP<span>01</span></div>
            <div class="txt">
                <div class="ttl">内容の確認と回答</div>
                <p class="p4">
                    一問一問、質問に答えていくだけで悩むことなく申告データをかんたんに作成することが可能です。
                </p>
            </div>
            <div class="img r2"><img src="/img/lp/step01.png" alt=""></div>
        </div></div>
        <div class="points-block"><div class="points-block-inner2">
            <div class="step">STEP<span>02</span></div>
            <div class="txt">
                <div class="ttl">添付書類提出</div>
                <p class="p4">
                    生保控除証明書・損保控除証明書、その他の添付書類を人事担当者に提出します。
                </p>
            </div>
            <div class="img r2"><img src="/img/lp/step02.png" alt=""></div>
        </div></div>
    </div>
</div></div>
<div class="section-inner p-b0" id="features02"><div class="wrap">
    <div class="block-ttl">
        <div class="num">2</div>
        <div class="ttl">
            <div class="sub">ペーパーレスで業務管理もかんたん・便利に！</div>
            <div class="main">人事担当者の業務を自動化</div>
        </div>
    </div>
    <div class="points-wrap c3">
        <div class="points-block c3 m-t0"><div class="points-block-inner3">
            <div class="img-area"><img src="/img/lp/step03.jpg" alt=""></div>
            <div class="txt">
                <div class="ttl t-c">従業員への申告書の<br>印刷・配布が不要</div>
                <p class="p0">
                    従業員への年末調整の通知や申告書の収集をシステム上でおこないます。申告書の印刷・配付・収集が不要になります。
                </p>
            </div>
        </div></div>
        <div class="points-block c3 m-t0"><div class="points-block-inner3">
            <div class="img-area"><img src="/img/lp/step04.jpg" alt=""></div>
            <div class="txt">
                <div class="ttl t-c">収集・確認状況が<br>一目でわかる</div>
                <p class="p0">
                    従業員全員の年末調整の状況が自動で数値化されます。提出された書類の集計や管理がラクになります。
                </p>
            </div>
        </div></div>
        <div class="points-block c3 m-t0"><div class="points-block-inner3">
            <div class="img-area"><img src="/img/lp/step05.jpg" alt=""></div>
            <div class="txt">
                <div class="ttl t-c">各種給与システムへの<br>連携が可能</div>
                <p class="p0">
                    集めた年末調整の情報を各種給与システムへデータで連携できます。申告書を見ながら手入力する手間が省けます。
                </p>
            </div>
        </div></div>
    </div>
</div></div>
<div class="section-inner p-b20" id="features03"><div class="wrap">
    <div class="block-ttl m-b0">
        <div class="num">3</div>
        <div class="ttl">
            <div class="sub">たった5分で導入可能！</div>
            <div class="main">即日機能をご利用いただけます。</div>
        </div>
    </div>
    <div class="c-img"><img src="/img/lp/step06.jpg" alt="PC(Win・Mac) スマートフォン(Android・iOS) 全対応"></div>
</div></div>
</section>
<section class="block " id="price">
<div class="section-inner p-b40"><div class="wrap">
    <div class="c-ttl"><div class="c-ttl-in">
        <h3>導入費用</h3>
    </div></div>
    <p class="note01 sp">※左右にスワイプすることで表の全体が見れます。</p>
    <div class="data-table-l separate s-over m-b80">
        <table>
            <thead>
                <tr>
                    <th class="bb0" width="200"><span>プラン</span></th>
                    <th class="bb0" width="330"><span>ご利用人数</span></th>
                    <th class="bb0" width="330"><span>年間利用料<span class="f-s80">(税抜)</span></span></th>
                    <th class="bb0" width="340"><span>機能制限</span></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="bb0 f-c6" rowspan="2"><span class="f-s130">正規版</span></th>
                    <td class="bb0 f-c6"><span class="f-s130 f-bold">25人以下</span>の場合</td>
                    <td class="bb0 f-c4"><span>一律 <span class="f-s130 f-bold">10,000円</span> ／年</span></td>
                    <th class="bb0 f-c6" rowspan="2"><span class="f-s130">なし</span></th>
                </tr>
                <tr>
                    <td class="bt1 btdot bb0 f-c6"><span class="f-s130 f-bold">26人以上</span>の場合</td>
                    <td class="bt1 btdot bb0 f-c4"><span>１人につき <span class="f-s130 f-bold">400円</span> ／年</span></td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="f-c1">
                    <th class="b-c9">
                        <span class="f-c6 f-s130">お試し版</span><br>
                        <span class="f-c6 dis-ib p-t5 f-s80 f-lighter">2018年版の様式での<br>ご提供となります。</span>
                    </th>
                    <td class="b-c9 f-c6"><span class="f-s140 f-bold">300人</span>まで</td>
                    <td class="b-c9"><span class="f-s140 f-bold">0円</span></td>
                    <td class="b-c9 f-c6"><span class="f-s130 f-bold">あり</span><br>
                        <span class="dis-ib p-t5 f-s80">データ出力（PDF･CSV･XML）外部連携<br>（他社システム連携）が行えません。</span>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div></div>
</section>
<section class="block bg-y">
<div class="section-inner"><div class="wrap">
    <div class="sub-ttl">
        まずは、お気軽にお問い合わせください！
    </div>
    <div class="c-btn s2">
        <a href="https://service.officestation.jp/guest/web-application/trial" target="_blank" class=""><span class="">30日間無料お試し</span></a>
        <a href="/document/" class="c2"><span class="">資料ダウンロード</span></a>
    </div>
</div></div>
</section>
<section class="block case-block" id="case">
<div class="section-inner p-t100"><div class="wrap">
    <div class="c-ttl m-b30">
        <h3>導入企業様8,000社突破！</h3>
    </div>
	<div class="c-img">
		<img src="/img/home/case_logos.png" alt="">
	</div>
</div></div>
</section>
<section class="block" id="">
<div class="section-inner p-b0"><div class="wrap">
    <div class="c-ttl"><div class="c-ttl-in">
        <div class="sub">安心・安全のクラウド型システム</div>
        <h3>金融機関並みのセキュリティシステム</h3>
    </div></div>
    <div class="feature-block">
		<div class="list">
			<ul>
				<li><div class="img" style="background-image: url(/img/lp/img_popup1-1.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>WAF<br class="pcbr"><span>（多重ファイアウォール設置）</span></div></div>
						<div class="txt">
							Webアプリケーションファイアーウォール（WAF）を設置し、不正アクセス、サイトの改ざん、情報の詐取を防ぎます。
						</div>
					</div>
				</li>
				<li><div class="img" style="background-image: url(/img/lp/img_popup1-2.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>二重認証</div></div>
						<div class="txt">
							仮に第三者がログインIDとパスワードの入手に成功したとしても、2段階目の認証（乱数表による認証）をクリアしなければアクセスすることはできません。
						</div>
					</div>
				</li>
				<li><div class="img" style="background-image: url(/img/lp/img_popup1-3.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>バックアップ不要</div></div>
						<div class="txt">
							クラウド上にデータを保管、面倒なバックアップはいりません。万が一パソコンに問題が生じても、業務に支障が生じません。
						</div>
					</div>
				</li>
			</ul>
			<ul>
				<li><div class="img" style="background-image: url(/img/lp/img_popup1-4.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>通信データと<br class="pcbr">サーバー本体を暗号化</div></div>
						<div class="txt">
							システムに登録された機密情報はすべて暗号化され、保存されています。<br>
							さらにサーバー本体でも暗号化を行っているため、非常に高いセキュリティ性を確保することができています。サーバーは日本国内のデータセンターで運用されています。
						</div>
					</div>
				</li>
				<li><div class="img" style="background-image: url(/img/lp/img_popup1-5.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>ISO 27001（ISMS）<br class="pcbr">を取得</div></div>
						<div class="txt">
							情報セキュリティマネジメントシステム（ISMS）の国際規格である「ISO/IEC27001」の認証を取得しております。情報の機密性・完全性・可用性の維持、改善に日々取り組んでいます。
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div></div>
</section>
<section class="block" id="">
<div class="section-inner p-b20"><div class="wrap">
    <div class="c-ttl"><div class="c-ttl-in">
        <div class="sub">簡単な操作と行き届いたフォロー体制</div>
        <h3>お客様の期待を“超える”サポートデスク</h3>
    </div></div>
    <div class="feature-block">
		<div class="list s2">
			<ul>
				<li><div class="img" style="background-image: url(/img/lp/img_popup2-1.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>待ち時間<br><span>0分</span></div></div>
						<div class="txt">
							従業員、顧問先のみなさまの大切な労務手続を行う業務システム。<br>労務手続きシステムで疑問・質問がございましたら待ち時間なく、サポートスタッフとお話しいただける環境をお約束します。
						</div>
					</div>
				</li>
				<li><div class="img" style="background-image: url(/img/lp/img_popup2-2.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>資格者<br><span>在 籍</span></div></div>
						<div class="txt">
							お客様の利用シーンに関連するシステム利用方法をより的確にお答えできるように、専門知識を有した社会保険労務士有資格者が在籍しています。お気軽に相談ください。
						</div>
					</div>
				</li>
				<li><div class="img" style="background-image: url(/img/lp/img_popup2-3.jpg);"></div>
					<div class="txt-area">
						<div class="ttl"><div>解決率<br><span>100%</span></div></div>
						<div class="txt">
							システム開発チームと同フロアにサポートデスクを設置し、迅速な社内連携を実現しています。日々進化するIT課題や法改正など、先んじて対策を講じお客様満足を高めています。
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div></div>
</section>
<section class="block border-b" id="price">
<div class="section-inner"><div class="wrap">
    <div class="c-ttl"><div class="c-ttl-in">
        <div class="sub">2020年こそウェブでスイスイスマートに！</div>
        <h3>ペーパーレス年末調整入門講座 開催中！</h3>
    </div></div>
    <div class="seminar-table">
        <div class="seminar-table-txt">
            <div class="t1">… 社労士向け</div>
            <div class="t2">… 企業向け</div>
        </div>
        <table>
            <thead>
                <tr>
                    <th class="th1">地域</th>
                    <th class="th2">対象</th>
                    <th class="th3">日時</th>
                    <th class="th4">セミナー内容</th>
                </tr>
            </thead>
            <tbody>
                                            <tr>
                                                <td class=""><div>オンライン
                                                </div></td>
                                                    <td class="t2"><div>企業向け</div></td>
                                                <td class=""><div><div class="date">
                                                    8月24日(月)<span>10:00～10:45</span>
                                                </div></div></td>
                                                <td class="txt seminar_ttl"><div>
                                                    <p><a href="/seminar/nencho-0824am/">2020年版完全対応！ 2020年こそウェブでスイスイスマートに！<br> ペーパーレス年末調整入門講座【オンライン】</a></p>
                                                </div></td>
                                            </tr>
                                            <tr>
                                                <td class=""><div>オンライン
                                                </div></td>
                                                    <td class="t2"><div>企業向け</div></td>
                                                <td class=""><div><div class="date">
                                                    8月24日(月)<span>15:00～15:45</span>
                                                </div></div></td>
                                                <td class="txt seminar_ttl"><div>
                                                    <p><a href="/seminar/nencho-0824pm/">2020年版完全対応！ 2020年こそウェブでスイスイスマートに！<br> ペーパーレス年末調整入門講座【オンライン】</a></p>
                                                </div></td>
                                            </tr>
            </tbody>
        </table>
    </div>
</div></div>
</section>
</main><!--======================================-->
<section class="block bg-y">
<div class="section-inner"><div class="wrap">
    <div class="sub-ttl">
        まずは、お気軽にお問い合わせください！
    </div>
    <div class="c-btn s2">
        <a href="https://service.officestation.jp/guest/web-application/trial" target="_blank" class=""><span class="">30日間無料お試し</span></a>
        <a href="/document/" class="c2"><span class="">資料ダウンロード</span></a>
    </div>
</div></div>
</section>
<footer><div class="footer">
<div class="wrap"><div class="footer-inner">
    <div class="footer-l">
        <div class="logo"><a href="https://www.fmltd.co.jp" target="_blank"><img src="/img/common/c_logo.png" alt="株式会社エフアンドエム"></a></div>
    </div>
    <div class="footer-r">
        <nav class="nav"><ul class="list">
            <li class="item"><a href="/">オフィスステーション TOP</a></li>
            <li class="item"><a href="/sla/">SLA</a></li>
            <li class="item"><a href="https://www.fmltd.co.jp" target="_blank">運営会社について</a></li>
        </ul></nav>
        <div class="copy">
            <a href="/">&copy; 2015 F&M co.,ltd.</a>
        </div>
    </div>
</div></div>
</div></footer>
<div id="pagetop"><a class="scroll" href="#top"><span></span></a></div>
</div><!--frame-outer-->
</body>
</html>
