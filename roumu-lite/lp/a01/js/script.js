//PC以外：ヘッダーの高さ分だけコンテンツを下げる
$(window).on("load", function () {
    if (window.matchMedia("(max-width: 1024px)").matches) {
        var h1_height = $("header h1").innerHeight();
        var h2_height = $("header h2").innerHeight();
        $(".mv_bg").css("margin-top", h1_height);
        $(".mv_bg").css("padding-top", h2_height + 15);
    }
});

$(function () {
    //モーダル画面
    $("#compare_btn").click(function () {
        $(".modal").fadeIn("fast");
    });
    $(".close_btn").click(function () {
        $(".modal").hide();
    });

    // セミナー情報リンク（PC）
    $(".fixed_links .pc .toggle_btn").click(function () {
        $(".fixed_links .pc").toggleClass("close");
    });
});
