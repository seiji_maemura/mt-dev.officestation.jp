<?php
        include '../module/form/main.php';
    ?>
    

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    
    <title>オフィスステーション 労務ライトお問い合わせ</title>
    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>

    <!-- All in One SEO Pack 3.2.4 によって Michael Torbert の Semper Fi Web Design[426,433] -->
    <link rel="canonical" href="https://www.officestation.jp/roumu-lite/contact.php" />
    <meta name="description"  content="" />
    <meta property="og:title" content="オフィスステーション 労務ライトお問い合わせ" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.officestation.jp/roumu-lite/contact.php" />
    <meta property="og:image" content="https://www.officestation.jp/roumu-lite/img/ogp.png" />
    <meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
    <meta property="og:image:secure_url" content="https://www.officestation.jp/roumu-lite/img/ogp.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="オフィスステーション 労務ライトお問い合わせ" />
    <meta name="twitter:image" content="https://www.officestation.jp/roumu-lite/img/ogp.png" />
    <!-- All in One SEO Pack -->

    <!-- clear a cache -->
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">

    <link rel='dns-prefetch' href='//s.w.org' />
    <!-- Markup (JSON-LD) structured in schema.org ver.4.6.5 START -->
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [
            {
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/",
                    "name": "オフィスステーション 労務ライト"
                }
            },
            {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/contact.php",
                    "name": "オフィスステーション 労務ライトお問い合わせ"
                }
            }
        ]
    }
    </script>
    <!-- Markup (JSON-LD) structured in schema.org END -->
    <style type="text/css">
        #loading {
            width: 100vw;
            height: 100vh;
            transition: all 1s;
            background-color: #fff;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100000000;
        }
    </style>
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/styles_sp.css">
    <link rel="stylesheet" href="/css/styles_.css">
    <link rel="stylesheet" href="/css/styles-t.css">
    <link rel="stylesheet" href="/css/styles_sp-t.css">
    <link rel="stylesheet" href="/css/styles_i.css">
    <link rel="stylesheet" href="/css/lp_common.css">
    <link rel="stylesheet" href="/css/lp_common_sp.css">
    <link rel="stylesheet" href="/module/form/form.css">

    <!-- JS Setting-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        $(function(){
        　var loading = $("#loading");
        　var isHidden = function(){
        　　loading.hide();
        　};
        　//0.5秒後にloadingFunc開始
        　setTimeout(isHidden,500);
        });
    </script>
    <script src="/roumu-lite/js/cache.js"></script>

    

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="loading"></div>

    <div id="frame-outer">
        <header>
            <div id="header" class="header">
                <div class="header-inner">
                    <div class="header-logo">
                        <a href="/roumu-lite/">
                            <div class="img" id="header-logo">
                                <div class="logo02 lite-logo"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </header>
    
        
        <div class="content">
        
            <div class="page-ttl">
                <h1>
                    <span>お問い合わせ</span>
                </h1>
            </div>
            <div id="bread">
                <ul>
                    <li><a href="/roumu-lite/">オフィスステーション 労務ライト</a></li>
                    <li>お問い合わせ</li>
                </ul>
            </div>
            <section class="block">
                <div class="section-inner">
                    <div class="wrap">
                        <div class="c-txt sp_t-l m-b60">
                            <p>
                                オフィスステーション 労務ライトは企業のお客様限定のサービスです。<br>
                                社会保険労務士・税理士の方はご利用になれませんのでご注意ください。<br>
                                社会保険労務士・税理士の方はこちら→<a href="https://www.officestation.jp/pro/" target="_blank" class="d_inline">[オフィスステーションPro]</a>
                            </p>
                        </div>
                        <?php if( $page_flag === 0 ): ?>
                            <form action="" method="post" class="form">
                                <?php if( !empty($error) ): ?>
                                    <ul class="error-list">
                                    <?php foreach( $error as $value ): ?>
                                        <li><?php echo $value; ?></li>
                                    <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                                <!-- 氏名記入 -->
                                <div class="form-heading">
                                    <label class="required">氏名</label>
                                    <div class="answer-box">
                                        <div class="name-box">
                                            <input class="form-control name1 is_input" type="text" id="name" name="last_name" placeholder="姓" value="<?php if( !empty($clean['last_name']) ) { echo $clean['last_name']; } ?>" required>
                                            <input class="form-control is_input" type="text" id="name" name="first_name" placeholder="名" value="<?php if( !empty($clean['first_name']) ) { echo $clean['first_name']; } ?>" required>
                                        </div>
                                    </div>
                                </div>    
                                <!-- 会社名記入 -->
                                <div class="form-heading">
                                    <label class="required">会社名</label>
                                    <div class="answer-box">
                                        <input class="form-control is_input" type="text" id="company-name" name="user_company" placeholder="例：株式会社○○" value="<?php if( !empty($clean['user_company']) ) { echo $clean['user_company']; } ?>" required>
                                    </div>
                                </div>
                                <!-- 資本金記入 -->
                                <div class="form-heading">
                                    <label class="required">資本金</label>
                                    <div class="answer-box">
                                        <select name="company_capital" id="company-capital" class="is_input" required>
                                            <?php if( !empty($company_capitals) ): ?>
                                                <?php foreach( $company_capitals as $company_capital ): ?>
                                                    <option value="<?php echo $company_capital; ?>" <?php if( !empty($clean['company_capital']) && $clean['company_capital'] === $company_capital ){ echo 'selected'; } ?>><?php echo $company_capital; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                <!--従業員数記入 -->
                                <div class="form-heading">
                                    <label class="required">従業員数</label>
                                    <div class="answer-box">
                                        <input class="form-control" type="text" id="number-of-employees" class="is_input" name="number_of_employees" placeholder="例：100" value="<?php if( !empty($clean['number_of_employees']) ) { echo $clean['number_of_employees']; } ?>" required>
                                    </div>
                                </div>
                                <!-- メールアドレス記入 -->
                                <div class="form-heading">
                                    <label class="required" for="mail">メールアドレス</label>
                                    <div class="answer-box">
                                        <input class="form-control is_input" type="text" id="mail" name="user_mail" placeholder="例：example@officestation.jp"  value="<?php if( !empty($clean['user_mail']) ) { echo $clean['user_mail']; } ?>" required>
                                    </div>
                                </div>
                                <!-- 電話番号記入 -->
                                <div class="form-heading">
                                    <label class="required" for="tel">電話番号</label>
                                    <div class="answer-box">
                                        <input class="form-control" type="text" id="tel" name="user_tel"  placeholder="例：000-000-0000"  value="<?php if( !empty($clean['user_tel']) ) { echo $clean['user_tel']; } ?>" required>
                                    </div>
                                </div>
                                <!-- お問い合わせ内容記入 -->
                                <div class="form-heading">
                                    <label class="required" for="msg">お問い合わせ内容</label>
                                    <div class="answer-box">
                                        <textarea id="msg" name="user_message" required><?php if( !empty($clean['user_message']) ) { echo $clean['user_message']; } ?></textarea>
                                    </div>
                                </div>
                                <!-- 広告アンケート記入 -->
                                <div class="form-heading inflow_source">
                                <label>「オフィスステーション 労務ライト」を知った理由</label>
                                    <div class="answer-box">
                                        <?php if( !empty($inflow_sources) ): ?>
                                            <?php foreach( $inflow_sources as $id => $value ): ?>
                                                <label for="<?php echo $id; ?>"><input id="<?php echo $id; ?>" type="radio" name="inflow_source" value="<?php echo $value; ?>" <?php if( !empty($clean['inflow_source']) && $clean['inflow_source'] === $value ){ echo 'checked'; } ?>><?php echo $value; ?></label>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <input type="text" name="othertext" id="othertext" value="<?php if( !empty($clean['othertext']) ) { echo $clean['othertext']; } ?>">
                                    </div>
                                </div>
                                <!-- プライバシー同意 -->
                                <div class="form-heading">
                                    <label class="required">プライバシーステートメントを確認し、同意します。</label>
                                    <div class="answer-box">
                                        <span class="value">
                                            <input type="checkbox" name="agree" id="agree" value="同意する" <?php if( $clean['agree'] === "同意する" ){ echo 'checked'; } ?> required>
                                            <label class="agree" for="agree">同意する</label>
                                        </span>
                                        <p class="description"><a href="https://www.officestation.jp/privacy-statement/" target="_blank">プライバシーステートメント</a></p>
                                    </div>
                                </div>
                                <!-- 私はロボットではありません -->
                                <div class="g-recaptcha" data-callback="clearcall" data-sitekey="<?php echo $site_key ?>"></div>
                                <!-- 確認画面へボタン -->
                                <p class="submit text-center">
                                    <input id="os-submit" type="submit" name="btn_confirm" class="button" value="確認画面へ" disabled>
                                </p>
                            </form>
                        <?php elseif( $page_flag === 1 ): ?>
                            <form action="contact-thanks.php" method="post">
                                <!-- 氏名表示 -->
                                <div class="form-heading">
                                    <label class="required">氏名</label>
                                    <p><?php echo $clean['last_name']. " ". $clean['first_name']; ?></p>
                                </div>    
                                <!-- 会社名表示 -->
                                <div class="form-heading">
                                    <label class="required">会社名</label>
                                    <p><?php echo $clean['user_company']; ?></p>
                                </div>
                                <!-- 資本金表示 -->
                                <div class="form-heading">
                                    <label class="required">資本金</label>
                                    <p><?php echo $clean['company_capital']; ?></p>
                                </div>
                                <!-- 従業員数表示 -->
                                <div class="form-heading">
                                    <label class="required">従業員数</label>
                                    <p><?php echo $clean['number_of_employees']; ?></p>
                                </div>
                                <!-- メールアドレス表示 -->
                                <div class="form-heading">
                                    <label class="required" for="mail">メールアドレス</label>
                                    <p><?php echo $clean['user_mail']; ?></p>
                                </div>
                                <!-- 電話番号表示 -->
                                <div class="form-heading">
                                    <label class="required" for="tel">電話番号</label>
                                    <p><?php echo $clean['user_tel']; ?></p>
                                </div>
                                <!-- お問い合わせ内容表示 -->
                                <div class="form-heading">
                                    <label class="required" for="msg">お問い合わせ内容</label>
                                    <p><?php echo $clean['user_message']; ?></p>
                                </div>
                                <!-- 広告アンケート表示 -->
                                <div class="form-heading">
                                    <label class="" for="inflow_source">「オフィスステーション 労務ライト」を知った理由</label>
                                    <p>
                                    <?php
                                        if( ($clean['inflow_source'] === "その他") && (!empty($clean['othertext'])) ) {
                                            $inflow_source = "その他:" .$clean['othertext'];
                                            $clean['inflow_source'] = $inflow_source;
                                        }
                                        echo $clean['inflow_source'];
                                    ?>
                                    </p>
                                </div>
                                <!-- プライバシー同意 -->
                                <div class="form-heading">
                                    <label class="required" for="">プライバシーステートメントを確認し、同意します。</label>
                                    <p><?php echo $clean['agree']; ?></p>
                                </div>
                                <!-- ボタン -->
                                <div class="btns">
                                    <input class="button" type="button" name="btn_back" value="戻る" onclick="history.back()">
                                    <input id="os-submit" class="button" type="submit" name="btn_submit" value="送信する">
                                </div>
                                <!-- hiddenでサンクス画面へデータ引き渡し -->
                                <input type="hidden" name="last_name" value="<?php echo $clean['last_name']; ?>">
                                <input type="hidden" name="first_name" value="<?php echo $clean['first_name']; ?>">
                                <input type="hidden" name="user_company" value="<?php echo $clean['user_company']; ?>">
                                <input type="hidden" name="company_capital" value="<?php echo $clean['company_capital']; ?>">
                                <input type="hidden" name="number_of_employees" value="<?php echo $clean['number_of_employees']; ?>">
                                <input type="hidden" name="user_mail" value="<?php echo $clean['user_mail']; ?>">
                                <input type="hidden" name="user_tel" value="<?php echo $clean['user_tel']; ?>">
                                <input type="hidden" name="user_message" value="<?php echo $clean['user_message']; ?>">
                                <input type="hidden" name="inflow_source" value="<?php echo $clean['inflow_source']; ?>">
                                <input type="hidden" name="agree" value="<?php echo $clean['agree']; ?>">
                            </form>
                        <?php endif; ?>
                    </div>
                </div>
            </section>

<footer>
                <div class="footer-logo"><a href="https://www.fmltd.co.jp" target="_blank"><img src="/img/common/c_logo.png" alt="株式会社エフアンドエム"></a></div>
                <div class="right">
                    <div class="link"><a href="https://www.officestation.jp/" target="_blank">オフィスステーションTOP</a></div>
                    <div class="link"><a href="https://www.officestation.jp/sla/" target="_blank">SLA</a></div>
                    <div class="link"><a href="https://www.officestation.jp/corporate/" target="_blank">運営会社について</a></div>
                    <div class="footer-copy"><p>&copy; 2015 F&M co.,ltd.</p></div>
                </div>
            </footer>
        </div>
    </div>


    <script src="/module/form/recaptcha.js"></script>

</body>
</html>