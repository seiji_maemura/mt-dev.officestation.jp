//PC以外：ヘッダーの高さ分だけコンテンツを下げる
$(window).on("load", function () {
    if (window.matchMedia("(max-width: 1024px)").matches) {
        var h1_height = $("header h1").innerHeight();
        var h2_height = $("header h2").innerHeight();
        $(".mv_bg").css("margin-top", h1_height);
        $(".mv_bg").css("padding-top", h2_height + 15);
    }
});

$(function () {
    //比較表モーダル画面
    $("#compare_btn1").click(function () {
        $(".modal1").fadeIn("fast");
    });
    $(".close_btn").click(function () {
        $(".modal1").hide();
    });

    $("#compare_btn2").click(function () {
        $(".modal2").fadeIn("fast");
    });
    $(".close_btn").click(function () {
        $(".modal2").hide();
    });

    // 固定セミナー情報リンク（PC）
    $(".fixed_links .pc .toggle_btn").click(function () {
        $(".fixed_links .pc").toggleClass("close");
    });
});
