// js,cssをキャッシュさせずに読み込む
function readEnvironment() {
    var link = document.createElement("link");
    link.href = "/roumu-lite/css/roumu-lite.css?" + new Date() / 1000;
    link.rel = "stylesheet";
    link.type = "text/css";
    document.getElementsByTagName("head")[0].appendChild(link);
}
readEnvironment();
