<?php
    include '../module/form/main.php';
?>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,700,900|Noto+Serif+JP|Roboto+Condensed:700&display=swap" rel="stylesheet">


    <meta name="robots" content="noindex,nofollow">
    <title>オフィスステーション 労務ライト資料ダウンロードありがとうございます。</title>
<!--sns-->
    <link rel="canonical" href="https://www.officestation.jp/roumu-lite/document.html" />
    <meta property="og:title" content="オフィスステーション 労務ライト資料ダウンロードありがとうございます。" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.officestation.jp/roumu-lite/" />
    <meta property="og:image" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
    <meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
    <meta property="article:published_time" content="2019-08-27T12:02:31Z" />
    <meta property="article:modified_time" content="2019-11-29T09:58:12Z" />
    <meta property="og:image:secure_url" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="オフィスステーション 労務ライト資料ダウンロードありがとうございます。" />
    <meta name="twitter:image" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />



    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
    <!-- clear a cache -->
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <link rel='dns-prefetch' href='//s.w.org' />
    <style type="text/css">
    div#toc_container {
        background: #f9f9f9;
        border: 1px solid #aaaaaa;
    }
    </style><!-- Markup (JSON-LD) structured in schema.org ver.4.6.5 START -->
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/",
                    "name": "オフィスステーション 労務ライト"
                }
            },
            {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/contact.html",
                    "name": > お問い合わせ "
                }
            }
        ]
    }
    </script>
    <!-- Markup (JSON-LD) structured in schema.org END -->

    <style type="text/css">
    .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
    }
    </style>
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/styles_sp.css">
    <link rel="stylesheet" href="/css/styles_.css">
    <link rel="stylesheet" href="/css/styles-t.css">
    <link rel="stylesheet" href="/css/styles_sp-t.css">
    <link rel="stylesheet" href="/css/styles_i.css">
    <link rel="stylesheet" href="/css/lp/common.css">
    <link rel="stylesheet" href="/css/lp/common_sp.css">
    <link rel="stylesheet" href="../module/form/form.css">
    <style type="text/css">
    #loading {
        width: 100vw;
        height: 100vh;
        transition: all 1s;
        background-color: #fff;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 100000000;
    }
    </style>

    <!-- JS Setting-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
    $(function() {
        var loading = $("#loading");
        var isHidden = function() {
            loading.hide();
        };
        //0.5秒後にloadingFunc開始
        setTimeout(isHidden, 500);
    });
    </script>
    <script src="js/cache.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
<!-- End Google Tag Manager -->
</head>

<body id="" class="">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div id="loading"></div>
    <div id="frame-outer">
        <section class="block block1">
            <header>
                <h1>
                    <a href="/" target="_blank"><img src="img/logo.png" class="logo" alt="オフィスステーション"></a>
                </h1>
                <h2><span>資本金1億円超</span>企業の社会保険・労働保険手続きの<span><span class="sp"><br></span>電子申請義務化に完全対応‼</span></h2>
                <a href="/roumu-lite/document.php" class="dwl">資料ダウンロード</a>
            </header>
        </section>

        <div class="content">
            <section class="document">
                <div class="ttl">
                    <h3>
                        オフィスステーション 労務ライト<br>
                        説明資料 / 人事労務のお役立ち資料
                    </h3>
                </div>
                <div id="bread"></div> 
                <div class="section-inner">
                    <div class="thanks-wrap">
                        <div class="txt">
                            <p>
                                このたびは、お問い合わせくださり、 誠にありがとうございます。<br>
                                お求めの資料は、下記リンクよりダウンロードしてください。
                            </p>
                        </div>
                        <ul class="box-list5">
                            <li class="box">
                                <h4 class="ttl"><span>オフィスステーション労務ライト説明資料</span></h4>
                                <div class="btn">
                                    <a href="https://go.officestation.jp/contents/files/roumulite_roumuliteguide.pdf" download="" target="_blank">
                                        <span>DOWNLOAD</span>
                                    </a>
                                </div>
                            </li>
                            <li class="box">
                                <h4 class="ttl"><span>2020年4月電子申請義務化～企業が行うべき対応～</span></h4>
                                <div class="btn">
                                    <a href="https://go.officestation.jp/contents/files/roumulite_ebook_electronicapplication.pdf" download="" target="_blank">
                                        <span>DOWNLOAD</span>
                                    </a>
                                </div>
                            </li>
                            <li class="box">
                                <h4 class="ttl"><span>年間スケジュールかんたんガイド</span></h4>
                                <div class="btn">
                                    <a href="https://go.officestation.jp/contents/files/roumulite_ebook_schedule.pdf" download="" target="_blank">
                                        <span>DOWNLOAD</span>
                                    </a>
                                </div>
                            </li>
                            <li class="box">
                                <h4 class="ttl"><span>帳票作成が30分 ⇒ 7分に!! ～e-Gov利用者の劇的な業務改善～</span></h4>
                                <div class="btn">
                                    <a href="https://go.officestation.jp/contents/files/roumulite_ebook_formcreation.pdf" download="" target="_blank">
                                        <span>DOWNLOAD</span>
                                    </a>
                                </div>
                            </li>
                            <li class="box">
                                <h4 class="ttl"><span>そもそもe-Govとは？ ～e-Govをはじめてご利用になる方へ～</span></h4>
                                <div class="btn">
                                    <a href="https://go.officestation.jp/files/roumulite_ebook_egov.pdf" download="" target="_blank">
                                        <span>DOWNLOAD</span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Pardotへのデータ連携 -->
                <?php if( !empty($_POST) && !empty($clean) ) {
                    $last_name = urlencode($clean['last_name']); 
                    $first_name = urlencode($clean['first_name']); 
                    $user_company = urlencode($clean['user_company']); 
                    $company_capital = urlencode($clean['company_capital']); 
                    $number_of_employees = urlencode($clean['number_of_employees']); 
                    $user_mail = urlencode($clean['user_mail']); 
                    $user_tel = urlencode($clean['user_tel']); 
                    $ads = urlencode($clean['ads']); 
                    $agree = urlencode($clean['agree']);

                    echo '<iframe id="iframe" src="https://go.officestation.jp/l/723363/2020-05-07/94rpp?last_name='.$last_name.'&first_name='.$first_name.'&user_company='.$user_company.'&company_capital='.$company_capital.'&number_of_employees='.$number_of_employees.'&user_mail='.$user_mail.'&user_tel='.$user_tel.'&ads='.$ads.'&agree='.$agree.'" width="1" height="1" style="display:none;"></iframe>';
                }
                ?>
            </section>
<footer>
            <div class="footer-logo"><a href="https://www.fmltd.co.jp" target="_blank"><img src="/img/common/c_logo.png" alt="株式会社エフアンドエム"></a></div>
            <div class="right">
                <div class="link"><a href="https://www.officestation.jp/sla/" target="_blank">SLA</a></div>
                <div class="link"><a href="https://www.officestation.jp/corporate/" target="_blank">運営会社について</a></div>
                <div class="footer-copy"><p>&copy; 2015 F&M co.,ltd.</p></div>
            </div>
        </footer>
    </div>
</div>

    <script src="../module/form/recaptcha.js"></script>

</body>
</html>