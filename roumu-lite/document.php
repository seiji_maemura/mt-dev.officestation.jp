<?php
    include '../module/form/main.php';
?>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,700,900|Noto+Serif+JP|Roboto+Condensed:700&display=swap" rel="stylesheet">


    <title>オフィスステーション 労務ライト資料ダウンロード</title>
<!--sns-->
    <link rel="canonical" href="https://www.officestation.jp/roumu-lite/document.html" />
    <meta property="og:title" content="オフィスステーション 労務ライト資料ダウンロード" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.officestation.jp/roumu-lite/" />
    <meta property="og:image" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
    <meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
    <meta property="article:published_time" content="2019-08-27T12:02:31Z" />
    <meta property="article:modified_time" content="2019-11-29T09:58:12Z" />
    <meta property="og:image:secure_url" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="オフィスステーション 労務ライト資料ダウンロード" />
    <meta name="twitter:image" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />



    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
    <!-- clear a cache -->
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <link rel='dns-prefetch' href='//s.w.org' />
    <style type="text/css">
    div#toc_container {
        background: #f9f9f9;
        border: 1px solid #aaaaaa;
    }
    </style><!-- Markup (JSON-LD) structured in schema.org ver.4.6.5 START -->
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/",
                    "name": "オフィスステーション 労務ライト"
                }
            },
            {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/contact.html",
                    "name": > お問い合わせ "
                }
            }
        ]
    }
    </script>
    <!-- Markup (JSON-LD) structured in schema.org END -->

    <style type="text/css">
        #loading {
            width: 100vw;
            height: 100vh;
            transition: all 1s;
            background-color: #fff;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100000000;
        }
    </style>
    <link rel="stylesheet" href="/css/styles-t.css">
    <link rel="stylesheet" href="/css/styles_sp-t.css">
    <link rel="stylesheet" href="../module/form/form.css">

    <!-- JS Setting-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
    $(function() {
        var loading = $("#loading");
        var isHidden = function() {
            loading.hide();
        };
        //0.5秒後にloadingFunc開始
        setTimeout(isHidden, 500);
    });
    </script>
    <script src="js/cache.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
<!-- End Google Tag Manager -->
</head>

<body id="" class="">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div id="loading"></div>
    <div id="frame-outer">
        <section class="block block1">
            <header>
                <h1>
                    <a href="/" target="_blank"><img src="img/logo.png" class="logo" alt="オフィスステーション"></a>
                </h1>
                <h2><span>資本金1億円超</span>企業の社会保険・労働保険手続きの<span><span class="sp"><br></span>電子申請義務化に完全対応‼</span></h2>
                <a href="/roumu-lite/document.php" class="dwl">資料ダウンロード</a>
            </header>
        </section>

        <div class="content">
            <section class="document">
                <div class="ttl">
                    <h3>
                        オフィスステーション 労務ライト<br>
                        説明資料 / 人事労務のお役立ち資料
                    </h3>
                </div>
                <div class="section-inner">

                    <?php if( $page_flag === 0 ): ?>
                        <div class="lists">
                            <ul class="wrap">
                                <li>
                                    <p class="num"><span>01</span></p>
                                    <img src="img/img09.jpg">
                                    <h4>オフィスステーション<br>労務ライト説明資料</h4>
                                    <div><p>オフィスステーション 労務ライトの概要と特徴をご説明いたします。</p></div>
                                </li>
                                <li>
                                    <p class="num"><span>02</span></p>
                                    <img src="img/img10.jpg">
                                <h4>2020年4月電子申請義務化<br><span>～企業が行うべき対応～</span></h4>
                                    <div>
                                        <ul>
                                            <li>2020年4月の電子申請義務化とは</li>
                                            <li>電子化がもたらす企業のメリット</li>
                                            <li>電子申請義務化で必要となる企業の対応</li>
                                            <li>オフィスステーションで電子化する業務</li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <p class="num"><span>03</span></p>
                                    <img src="img/img11.jpg">
                                    <h4>年間スケジュール<br>かんたんガイド</h4>
                                    <div>
                                        <p>おさえておきたい年間スケジュール<br>
                                        主軸となる毎月の給与の計算と支払い事務、毎年行うべき業務に加えて、勤怠管理や社員の入退社、結婚・出産関連などの不定期な業務についてまとめています。</p>
                                    </div>
                                </li>
                                <li>
                                    <p class="num"><span>04</span></p>
                                    <img src="img/img15.png">
                                    <h4>
                                        帳票作成が30分 ⇒ 7分に!!<br><span>～e-Gov利用者の劇的な業務改善～</span>
                                    </h4>
                                    <div>
                                        <p>なぜe-Govの直接利用よりAPI申請のほうが多いのか、e-Govの直接利用とAPI申請では何が違うのか、ご説明します。</p>                            </p>
                                    </div>
                                </li>
                                <li>
                                    <p class="num"><span>05</span></p>
                                    <img src="img/img16.png">
                                    <h4>
                                        そもそもe-Govとは？<br><span>～e-Govをはじめてご利用になる方へ～</span>
                                    </h4>
                                    <div>
                                        <p>本資料では、
                                            <ul>
                                                <li>そもそもe-Govのことがよくわからない</li>
                                                <li>e-Govを利用するうえで必要な準備がわからない</li>
                                                <li>e-Govを利用するうえでの注意点を知りたい</li>
                                            </ul>
                                            などの悩みを解決し、e-Govをより利用しやすくなる方法もご案内いたします。
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="form-wrap">
                            <p>以下に必要事項をご記入の上、確認画面より[ダウンロード]ボタンをクリックしていただきますと<span class="pc"><br></span>資料ダウンロードページに遷移いたします。</p>
                            
                            <form action="" method="post" class="form">

                                <?php if( !empty($error) ): ?>
                                    <ul class="error-list">
                                    <?php foreach( $error as $value ): ?>
                                        <li><?php echo $value; ?></li>
                                    <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>

                                <!-- 氏名記入 -->
                                <div class="form-heading">
                                    <label class="required">氏名</label>
                                    <div class="answer-box">
                                        <div class="name-box">
                                            <input class="form-control name1 is_input" type="text" id="name" name="last_name" placeholder="姓" value="<?php if( !empty($clean['last_name']) ) { echo $clean['last_name']; } ?>" required>
                                            <input class="form-control is_input" type="text" id="name" name="first_name" placeholder="名" value="<?php if( !empty($clean['first_name']) ) { echo $clean['first_name']; } ?>" required>
                                        </div>
                                    </div>
                                </div>    
                                
                                <!-- 会社名記入 -->
                                <div class="form-heading">
                                    <label class="required">会社名</label>
                                    <div class="answer-box">
                                        <input class="form-control is_input" type="text" id="company-name" name="user_company" placeholder="例：株式会社○○" value="<?php if( !empty($clean['user_company']) ) { echo $clean['user_company']; } ?>" required>
                                    </div>
                                </div>
                                
                                <!-- 資本金記入 -->
                                <div class="form-heading">
                                    <label class="required">資本金</label>
                                    <div class="answer-box">
                                        <select name="company_capital" id="company-capital" required>
                                            <?php if( !empty($company_capitals) ): ?>
                                                <?php foreach( $company_capitals as $company_capital ): ?>
                                                    <option value="<?php echo $company_capital; ?>" <?php if( !empty($clean['company_capital']) && $clean['company_capital'] === $company_capital ){ echo 'selected'; } ?>><?php echo $company_capital; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>

                                <!--従業員数記入 -->
                                <div class="form-heading">
                                    <label class="required">従業員数</label>
                                    <div class="answer-box">
                                        <input class="form-control" type="text" id="number-of-employees" name="number_of_employees" placeholder="例：100" value="<?php if( !empty($clean['number_of_employees']) ) { echo $clean['number_of_employees']; } ?>" required>
                                    </div>
                                </div>
                                
                                <!-- メールアドレス記入 -->
                                <div class="form-heading">
                                    <label class="required" for="mail">メールアドレス</label>
                                    <div class="answer-box">
                                        <input class="form-control is_input" type="text" id="mail" name="user_mail" placeholder="例：example@officestation.jp"  value="<?php if( !empty($clean['user_mail']) ) { echo $clean['user_mail']; } ?>" required>
                                    </div>
                                </div>

                                <!-- 電話番号記入 -->
                                <div class="form-heading">
                                    <label class="required" for="tel">電話番号</label>
                                    <div class="answer-box">
                                        <input class="form-control" type="text" id="tel" name="user_tel"  placeholder="例：000-000-0000"  value="<?php if( !empty($clean['user_tel']) ) { echo $clean['user_tel']; } ?>" required>
                                    </div>
                                </div>

                                <!-- 広告記入 -->
                                <div class="form-heading ads">
                                    <label class="">最も印象に残った広告を選択してください（重複回答なし）</label>
                                    <div class="answer-box">
                                        <?php if( !empty($ads) ): ?>
                                            <?php foreach( $ads as $id => $value ): ?>
                                                <label for="<?php echo $id; ?>"><input id="<?php echo $id; ?>" type="radio" name="ads" value="<?php echo $value; ?>" <?php if( !empty($clean['ads']) && $clean['ads'] === $value ){ echo 'checked'; } ?>><?php echo $value; ?></label>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <input type="text" name="othertext" id="othertext" value="<?php if( !empty($clean['othertext']) ) { echo $clean['othertext']; } ?>">
                                    </div>
                                </div>

                                <!-- プライバシー同意 -->
                                <div class="form-heading">
                                    <label class="required">プライバシーステートメントを確認し、同意します。</label>
                                    <div class="answer-box">
                                        <span class="value">
                                            <input type="checkbox" name="agree" id="agree" value="同意する" <?php if( $clean['agree'] === "同意する" ){ echo 'checked'; } ?> required>
                                            <label class="agree" for="agree">同意する</label>
                                        </span>
                                        <p class="description"><a href="https://www.officestation.jp/privacy-statement/" target="_blank">プライバシーステートメント</a></p>
                                    </div>
                                </div>

                                <!-- 私はロボットではありません -->
                                <div class="g-recaptcha" data-callback="clearcall" data-sitekey="<?php echo $site_key ?>"></div>

                                <!-- 確認画面へボタン -->
                                <p class="submit text-center">
                                    <input id="os-submit" type="submit" name="btn_confirm" class="button" value="確認画面へ" disabled>
                                </p>
                                
                                <p class="contact">[   ご不明点はお気軽に<a href="/roumu-lite/contact.php">お問い合わせ</a>ください   ]</p>

                            </form>
                        </div>

                    <?php elseif( $page_flag === 1 ): ?>
                        <form action="document-thanks.php" method="post">

                            <!-- 氏名表示 -->
                            <div class="form-heading">
                                <label class="required">氏名</label>
                                <p><?php echo $clean['last_name']. " ". $clean['first_name']; ?></p>
                            </div>    
                            
                            <!-- 会社名表示 -->
                            <div class="form-heading">
                                <label class="required">会社名</label>
                                <p><?php echo $clean['user_company']; ?></p>
                            </div>

                            <!-- 資本金表示 -->
                            <div class="form-heading">
                                <label class="required">資本金</label>
                                <p><?php echo $clean['company_capital']; ?></p>
                            </div>

                            <!-- 従業員数表示 -->
                            <div class="form-heading">
                                <label class="required">従業員数</label>
                                <p><?php echo $clean['number_of_employees']; ?></p>
                            </div>

                            <!-- メールアドレス表示 -->
                            <div class="form-heading">
                                <label class="required" for="mail">メールアドレス</label>
                                <p><?php echo $clean['user_mail']; ?></p>
                            </div>

                            <!-- 電話番号表示 -->
                            <div class="form-heading">
                                <label class="required" for="tel">電話番号</label>
                                <p><?php echo $clean['user_tel']; ?></p>
                            </div>

                            <!-- 広告表示 -->
                            <div class="form-heading">
                                <label class="" for="ads">最も印象に残った広告を選択してください（重複回答なし）</label>
                                <p>
                                <?php
                                    if( ($clean['ads'] === "その他") && (!empty($clean['othertext'])) ) {
                                        $ads = "その他:" .$clean['othertext'];
                                        $clean['ads'] = $ads;
                                    }
                                    echo $clean['ads'];
                                ?>
                                </p>
                            </div>

                            <!-- プライバシー同意 -->
                            <div class="form-heading">
                                <label class="required" for="">プライバシーステートメントを確認し、同意します。</label>
                                <p><?php echo $clean['agree']; ?></p>
                            </div>

                            <!-- ボタン -->
                            <div class="btns">
                                <input class="button" type="button" name="btn_back" value="戻る" onclick="history.back()">
                                <input id="os-submit" class="button" type="submit" name="btn_submit" value="ダウンロード">
                            </div>

                            <!-- hiddenでサンクス画面へデータ引き渡し -->
                            <input type="hidden" name="last_name" value="<?php echo $clean['last_name']; ?>">
                            <input type="hidden" name="first_name" value="<?php echo $clean['first_name']; ?>">
                            <input type="hidden" name="user_company" value="<?php echo $clean['user_company']; ?>">
                            <input type="hidden" name="company_capital" value="<?php echo $clean['company_capital']; ?>">
                            <input type="hidden" name="number_of_employees" value="<?php echo $clean['number_of_employees']; ?>">
                            <input type="hidden" name="user_mail" value="<?php echo $clean['user_mail']; ?>">
                            <input type="hidden" name="user_tel" value="<?php echo $clean['user_tel']; ?>">
                            <input type="hidden" name="ads" value="<?php echo $clean['ads']; ?>">
                            <input type="hidden" name="agree" value="<?php echo $clean['agree']; ?>">

                        </form>

                    <?php endif; ?>
                        
                </div>
            </section>
<footer>
            <div class="footer-logo"><a href="https://www.fmltd.co.jp" target="_blank"><img src="/img/common/c_logo.png" alt="株式会社エフアンドエム"></a></div>
            <div class="right">
                <div class="link"><a href="https://www.officestation.jp/sla/" target="_blank">SLA</a></div>
                <div class="link"><a href="https://www.officestation.jp/corporate/" target="_blank">運営会社について</a></div>
                <div class="footer-copy"><p>&copy; 2015 F&M co.,ltd.</p></div>
            </div>
        </footer>
    </div>
</div>

    <script src="../module/form/recaptcha.js"></script>

</body>
</html>