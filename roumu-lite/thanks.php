<?php
    include '../module/form/main.php';
?>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP:300,400,700,900|Noto+Serif+JP|Roboto+Condensed:700&display=swap" rel="stylesheet">


    <meta name="robots" content="noindex,nofollow">
    <title>オフィスステーション 労務ライトお申し込みありがとうございます。</title>
<!--sns-->
    <link rel="canonical" href="https://www.officestation.jp/roumu-lite/form-thanks.html" />
    <meta property="og:title" content="オフィスステーション 労務ライトお申し込みありがとうございます。" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.officestation.jp/roumu-lite/" />
    <meta property="og:image" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
    <meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
    <meta property="article:published_time" content="2019-08-27T12:02:31Z" />
    <meta property="article:modified_time" content="2019-11-29T09:58:12Z" />
    <meta property="og:image:secure_url" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="オフィスステーション 労務ライトお申し込みありがとうございます。" />
    <meta name="twitter:image" content="https://www.officestation.jp/wp_cms/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png" />



    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
    <!-- clear a cache -->
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <link rel='dns-prefetch' href='//s.w.org' />
    <style type="text/css">
    div#toc_container {
        background: #f9f9f9;
        border: 1px solid #aaaaaa;
    }
    </style><!-- Markup (JSON-LD) structured in schema.org ver.4.6.5 START -->
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/",
                    "name": "オフィスステーション 労務ライト"
                }
            },
            {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "https://www.officestation.jp/roumu-lite/contact.html",
                    "name": > お問い合わせ "
                }
            }
        ]
    }
    </script>
    <!-- Markup (JSON-LD) structured in schema.org END -->

    <style type="text/css">
    .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
    }
    </style>
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/styles_sp.css">
    <link rel="stylesheet" href="/css/styles_.css">
    <link rel="stylesheet" href="/css/styles-t.css">
    <link rel="stylesheet" href="/css/styles_sp-t.css">
    <link rel="stylesheet" href="/css/styles_i.css">
    <link rel="stylesheet" href="/css/lp/common.css">
    <link rel="stylesheet" href="/css/lp/common_sp.css">
    <link rel="stylesheet" href="../module/form/form.css">
    <style type="text/css">
    #loading {
        width: 100vw;
        height: 100vh;
        transition: all 1s;
        background-color: #fff;
        position: fixed;
        top: 0;
        left: 0;
        z-index: 100000000;
    }
    </style>

    <!-- JS Setting-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script>
    $(function() {
        var loading = $("#loading");
        var isHidden = function() {
            loading.hide();
        };
        //0.5秒後にloadingFunc開始
        setTimeout(isHidden, 500);
    });
    </script>
    <script src="js/cache.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
<!-- End Google Tag Manager -->
</head>

<body id="" class="">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<div id="loading"></div>
<div id="frame-outer">
    <header>
        <div id="header" class="header">
            <div class="header-inner">
                <div class="header-logo">
                    <a href="/" target="_blank">
                        <div class="img" id="header-logo">
                            <div class="logo02"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <div class="content">
        <div class="page-ttl">
            <h1>
                <span>お申し込み</span>
            </h1>
        </div>
        <div id="bread">
            <ul>
                <li><a href="/roumu-lite/">オフィスステーション 労務ライト</a></li>
                <li>お申し込み</li>
            </ul>
        </div>
        <section class="block">
            <div class="section-inner">
                <div class="wrap">
                    <div class="c-ttl2 m-t30 m-b80 sp_m-b40">
                        <div class="main">
                            <h2>お申し込みを承りました。</h2>
                        </div>
                    </div>
                    <div class="c-txt m-b40">
                        <p><span class="dis-ib t-l">オフィスステーション 労務ライトに申し込みいただきありがとうございます。<br>サービスのご利用が可能になりましたら、順次ご登録いただいたメールアドレス宛にログイン情報をお送りいたします。<br>
                        今後とも「オフィスステーション」をよろしくお願い申し上げます。</span></p>
                    </div>
                </div>
            </div>
        </section>
<footer>
            <div class="footer-logo"><a href="https://www.fmltd.co.jp" target="_blank"><img src="/img/common/c_logo.png" alt="株式会社エフアンドエム"></a></div>
            <div class="right">
                <div class="link"><a href="https://www.officestation.jp/sla/" target="_blank">SLA</a></div>
                <div class="link"><a href="https://www.officestation.jp/corporate/" target="_blank">運営会社について</a></div>
                <div class="footer-copy"><p>&copy; 2015 F&M co.,ltd.</p></div>
            </div>
        </footer>
    </div>
</div>

</body>
</html>