$(function(){
   // not found 告知の初見非表示
    $('.seminar-notfound').css('display','none');
   // セミナーフィルタリング用テンプレート
   function filteringIn(n) {
      $('.seminar-list-block').css('display','none');
      var cnt = $(".seminar-list-cat-block ul li.active").length; // activeの数をチェックする
      var cnt_s1 = $(".filtering_s1").length; // クラス数を確認
      var cnt_s2 = $(".filtering_s2").length; // クラス数を確認
      if (cnt === 1) { // 1つのactiveがある（地域か対象のいずれか）
          $('.filtering_s1').fadeIn( 500 ); // filteringクラスがあるセミナーを表示
          $('.filtering_s2').fadeIn( 500 ); // filteringクラスがあるセミナーを表示
          // 該当セミナーがない場合の処理↓
          if (cnt_s1 === 0) { // s1が0の場合
            if (cnt_s2 === 0) { // s2も0の場合
                $('.seminar-notfound').fadeIn( 500 );
                $('.seminar-notfound span').text('現在対象のセミナーはありません。');
            }
          }
      } else if (cnt === 2) { // 2つactiveがある（地域と対象の2つ）
          $('.filtering_s1.filtering_s2').fadeIn( 500 ); // filteringクラスが2つあるセミナーを表示
          // 該当セミナーがない場合の処理↓
          // 2つアクティブがあるパターンはs1、s2共に0か、s1のみ0かs2のみ0かの3拓
          if (cnt_s1 === 0) { // s1が0の場合
            if (cnt_s2 === 0) { // s2も0の場合 いずれも0
                $('.seminar-notfound').fadeIn( 500 );
                $('.seminar-notfound span').text('現在対象のセミナーはありません。');
            } else { // s2は0ではない s1は0
                $('.seminar-notfound').fadeIn( 500 );
                $('.seminar-notfound span').text('現在対象のセミナーはありません。');
            }
          } else { // s1は0ではない
            if (cnt_s2 === 0) { // s2は0
                $('.seminar-notfound').fadeIn( 500 );
                $('.seminar-notfound span').text('現在対象のセミナーはありません。');
            }
          }
      } else { // 1つもactiveがない
          $('.seminar-list-block').fadeIn( 500 );
      }
  }
  // 各クリックした処理↓
  $('#tokyo span').click(function() { // 東京
      if ($(this).parent('li').hasClass('active')) { // ON の時に OFF に 
          $(this).parent('li').removeClass("active");
          $('.seminar-list-block').removeClass("filtering_s1");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      } else { // OFF のときに ON に
          $(".seminar-list-cat-block.s1 ul li").removeClass("active");
          $(this).parent('li').addClass("active");
          $('.seminar-list-block').removeClass("filtering_s1");
          $('.seminar-list-block.tokyo').addClass("filtering_s1");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      }
  });
  $('#osaka span').click(function() { // 大阪
      if ($(this).parent('li').hasClass('active')) { // ON の時に OFF に
          $(this).parent('li').removeClass("active");
          $('.seminar-list-block').removeClass("filtering_s1");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      } else { // OFF のときに ON に
          $(".seminar-list-cat-block.s1 ul li").removeClass("active");
          $(this).parent('li').addClass("active");
          $('.seminar-list-block').removeClass("filtering_s1");
          $('.seminar-list-block.osaka').addClass("filtering_s1");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      }
  });
  $('#others span').click(function() { // その他
      if ($(this).parent('li').hasClass('active')) { // ON の時に OFF に
          $(this).parent('li').removeClass("active"); 
          $('.seminar-list-block').removeClass("filtering_s1");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      } else { // OFF のときに ON に
          $(".seminar-list-cat-block.s1 ul li").removeClass("active");
          $(this).parent('li').addClass("active");
          $('.seminar-list-block').removeClass("filtering_s1");
          $('.seminar-list-block.others').addClass("filtering_s1");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      }
  });
  $('#company span').click(function() { // 企業向け
      if ($(this).parent('li').hasClass('active')) { // ON の時に OFF に
          $(this).parent('li').removeClass("active");
          $('.seminar-list-block').removeClass("filtering_s2");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      } else { // OFF のときに ON に
          $(".seminar-list-cat-block.s2 ul li").removeClass("active");
          $(this).parent('li').addClass("active");
          $('.seminar-list-block').removeClass("filtering_s2");
          $('.seminar-list-block.company').addClass("filtering_s2");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      }
  });
  $('#labor span').click(function() { // 社労士向け 
      if ($(this).parent('li').hasClass('active')) { // ON の時に OFF に
          $(this).parent('li').removeClass("active");
          $('.seminar-list-block').removeClass("filtering_s2");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      } else { // OFF のときに ON に
          $(".seminar-list-cat-block.s2 ul li").removeClass("active");
          $(this).parent('li').addClass("active");
          $('.seminar-list-block').removeClass("filtering_s2");
          $('.seminar-list-block.labor').addClass("filtering_s2");
          $('.seminar-notfound').css('display','none');
          filteringIn();
      }
  });
});