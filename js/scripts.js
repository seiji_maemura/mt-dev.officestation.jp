// SP_TEL
$(function() {
   if (!isPhone())
      return;
   $('span[data-action=call]').each(function() {
      var $ele = $(this);
      $ele.wrap('<a href="tel:' + $ele.data('tel') + '"></a>');
   });
});
function isPhone() {
   return (navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('Android') > 0);
}

$(function() {
   //リンク追加
   $(".header-nav  div[href]").click(function(){
      location.href = $(this).attr("href");
   });

   //リンク追加
   $(".case-block div[href]").click(function(){
      location.href = $(this).attr("href");
   });

   //アコーディオン
  $(".ac dd").hide();
  $(".ac.open dd").show();
  $(".ac dt").click(function(){
    $(this).next().slideToggle("fast");
    $(this).parent(".ac").toggleClass("open");
  });

   $(".register-block-list .more-btn").click(function(){
      $(this).prev().slideToggle("fast");
         $(this).toggleClass("close");
   });

  $(".news .ac .more").hide();
  $(".news .ac.open .more").show();
  $(".news .ac .body").click(function(){
    $(this).next().slideToggle("fast");
    $(this).parent(".news .ac").toggleClass("open");
  });

   //スムーススクロール
   var w = $(window).width();
   var x = 767;
   if (w <= x) {
      $('a[href^=#].scroll').click(function() {
         var speed = 500;
         var href= $(this).attr("href");
         var target = $(href == "#" || href == "" ? 'html' : href);
         var position = target.offset().top;
         $('body,html').animate({scrollTop:position}, speed, 'swing');
         return false;
      });
      $('a[href^=#].scroll2').click(function() {
         var speed = 500;
         var href= $(this).attr("href");
         var target = $(href == "#" || href == "" ? 'html' : href);
         var position = target.offset().top;
         $('body,html').animate({scrollTop:position-50}, speed, 'swing');
         return false;
      });
   } else {
      $('a[href^=#].scroll').click(function() {
         var speed = 500;
         var href= $(this).attr("href");
         var target = $(href == "#" || href == "" ? 'html' : href);
         var position = target.offset().top;
         $('body,html').animate({scrollTop:position}, speed, 'swing');
         return false;
      });
      $('a[href^=#].scroll2').click(function() {
         var speed = 500;
         var href= $(this).attr("href");
         var target = $(href == "#" || href == "" ? 'html' : href);
         var position = target.offset().top;
         $('body,html').animate({scrollTop:position-100}, speed, 'swing');
         return false;
      });
   }

   // グローバルナビ
   $('#header-menu').click(function() {
      $("#g-nav").fadeIn("fast");
   });
   $('#g-nav-close').click(function() {
      $("#g-nav").hide();
   });

   //PC時のみヘッダークラス追加
   $( "document" ).ready( function() {
      var winW = $(window).width();
      var devW = 767;
      if (winW >= devW) {
         var $win = $(window),
         $header = $('#header'),
         $bodyid = $('body[id]');
         $win.on('load scroll', function() {
            var value = $(this).scrollTop();
            if ( $bodyid = 'home' && value > 90 ) {
               $header.addClass('scrolledNav');
            } else if ( $bodyid = 'home' && value < 90 ){
               $header.removeClass('scrolledNav');
            }
         });
      }
   });

   // js-toggle
   $(window).on('load resize', function(){
      var winW = $(window).width();
      var devW = 767;
      if (winW <= devW) {
         $('.js-toggle-tit').off("click").click(function(){
         $(this).next('.js-toggle-content').slideToggle("fast");
         });
      }
   });

   //モーダル
   $("#modal-open").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main").fadeIn("slow");
      $("#modal-bg,#modal-main").click(function(){
         $("#modal-main,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main").outerWidth();
         var ch = $("#modal-main").outerHeight();
            $("#modal-main").css({
               "left": ((w - cw)/2) + "px",
               "top": ((h - ch)/2) + "px"
         });
      }
   });

   // CM用モーダル追加
   $("#modal-open-1").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main-1").fadeIn("slow");
      $("#modal-bg,#modal-main-1").click(function(){
         $("#modal-main-1,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main-1").outerWidth();
         var ch = $("#modal-main-1").outerHeight();
            $("#modal-main-1").css({
            "left": ((w - cw)/2) + "px",
            "top": ((h - ch)/2) + "px"
         });
      }
   });
   $("#modal-open-2").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main-2").fadeIn("slow");
      $("#modal-bg,#modal-main-2").click(function(){
         $("#modal-main-2,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main-2").outerWidth();
         var ch = $("#modal-main-2").outerHeight();
            $("#modal-main-2").css({
            "left": ((w - cw)/2) + "px",
            "top": ((h - ch)/2) + "px"
         });
      }
   });
   $("#modal-open-3").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main-3").fadeIn("slow");
      $("#modal-bg,#modal-main-3").click(function(){
         $("#modal-main-3,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main-3").outerWidth();
         var ch = $("#modal-main-3").outerHeight();
            $("#modal-main-3").css({
            "left": ((w - cw)/2) + "px",
            "top": ((h - ch)/2) + "px"
         });
      }
   });
   $("#modal-open-4").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main-4").fadeIn("slow");
      $("#modal-bg,#modal-main-4").click(function(){
         $("#modal-main-4,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main-4").outerWidth();
         var ch = $("#modal-main-4").outerHeight();
            $("#modal-main-4").css({
            "left": ((w - cw)/2) + "px",
            "top": ((h - ch)/2) + "px"
         });
      }
   });
   $("#modal-open-5").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main-5").fadeIn("slow");
      $("#modal-bg,#modal-main-5").click(function(){
         $("#modal-main-5,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main-5").outerWidth();
         var ch = $("#modal-main-5").outerHeight();
            $("#modal-main-5").css({
            "left": ((w - cw)/2) + "px",
            "top": ((h - ch)/2) + "px"
         });
      }
   });
   $("#modal-open-6").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main-6").fadeIn("slow");
      $("#modal-bg,#modal-main-6").click(function(){
         $("#modal-main-6,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main-6").outerWidth();
         var ch = $("#modal-main-6").outerHeight();
            $("#modal-main-6").css({
            "left": ((w - cw)/2) + "px",
            "top": ((h - ch)/2) + "px"
         });
      }
   });
   $("#modal-open-7").click(function(){
      $("body").append('<div id="modal-bg"></div>');
      modalResize();
      $("#modal-bg,#modal-main-7").fadeIn("slow");
      $("#modal-bg,#modal-main-7").click(function(){
         $("#modal-main-7,#modal-bg").fadeOut("slow",function(){
            $('#modal-bg').remove() ;
         });
      });
      $(window).resize(modalResize);
      function modalResize(){
         var w = $(window).width();
         var h = $(window).height();
         var cw = $("#modal-main-7").outerWidth();
         var ch = $("#modal-main-7").outerHeight();
            $("#modal-main-7").css({
            "left": ((w - cw)/2) + "px",
            "top": ((h - ch)/2) + "px"
         });
      }
   });
   // フォーム箇所フォント変更
   $("iframe").on("load",function(){
      $("iframe").contents().find('#contentMain').css("font-size","16px");
   });
});

$(window).on('load', function() {
   var url = $(location).attr('href');
   setTimeout(function(){
      if(url.indexOf("?id=") != -1){
         var id = url.split("?id=");
         var $target = $('#' + id[id.length - 1]);
         if($target.length){
            var pos = $target.offset().top;
            $("html, body").animate({scrollTop:pos-100}, 300);
         }
      }
   },0);
});

$(function() {
    $(".products-tab li a").click(function(){
        $(".products-tab li").removeClass("active");
        $(this).parent(".products-tab li").addClass("active");
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        if( href == "#products_all") {
            $(".products-body").show();
            return false;
        } else {
            $(".products-body").hide();
            $(target).show();
            return false;
        }
    });
    $(".products-switch li a").click(function(){
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        $(this).parent(".products-switch li").toggleClass("active");
        $(target).toggleClass("active");
        if ($(this).parents(".products-switch").find("li").hasClass("active")) {
            $(this).parents(".products-body").find(".products-body-block-none").hide();
        } else {
            $(this).parents(".products-body").find(".products-body-block-none").show();
        }
        return false;
    });

    $('#header-menu, #header-menu-f').click(function() {
        $("#g-nav-r").fadeIn("fast");
    });
    $('#g-nav-close').click(function() {
        $("#g-nav-r").hide();
    });

    // ハンバーガーメニュー内製品、9つの神対応次階層メニュークリック挙動
    $('#gl1').click(function() {
      $(this).find('.g-nav-subcontent').toggle('fast');
      if($('#gl1').find('.g-nav-subcontent').css('display')=='block'){
        if($('#gl3').find('.g-nav-subcontent').css('display')=='block'){
          $('#gl3 .g-nav-subcontent').toggle('fast');
        }
      }
    });
    $('#gl3').click(function() {
      $(this).find('.g-nav-subcontent').toggle('fast');
      if($('#gl1').find('.g-nav-subcontent').css('display')=='block'){
        if($('#gl3').find('.g-nav-subcontent').css('display')=='block'){
          $('#gl1 .g-nav-subcontent').toggle('fast');
        }
      }
    });
});

$(function(){
   $('#feature-pop01').click(function() {
         $("#feature-block01").fadeIn("fast");
   });
   $('.feature-block-close').click(function() {
         $(".feature-block").hide();
   });
});
$(function(){
   $('#feature-pop02').click(function() {
         $("#feature-block02").fadeIn("fast");
   });
   $('.feature-block-close').click(function() {
         $(".feature-block").hide();
   });
});

$(window).on('load resize', function(){
    var winW2 = $(window).width();
    var devW2 = 960;
    if (winW2 >= devW2) {
        $(window).scroll(function () {
            var s = $(this).scrollTop();
            var m = 300;
            if(s > m) {
                $("#header-f").fadeIn('400');
                //$("#f-footer").fadeIn('400');
            } else if(s < m) {
                $("#header-f").fadeOut('200');
                //$("#f-footer").fadeOut('200');
            }
        });
        //$("#header-f").show();
    } else {
        $("#header-f").hide();
        //$("#f-footer").hide();
    }
});
$(window).on('load resize', function(){
    var winW2 = $(window).width();
    var devW2 = 960;
    if (winW2 >= devW2) {

       $(window).scroll(function () {
          var s = $(this).scrollTop();
          var m = 720;
          if(s > m) {
             $(".faq-float").fadeIn(500);
          } else if(s < m) {
             $(".faq-float").fadeOut("fast");
          }
       });

    } else {

       $(window).scroll(function () {
          var s = $(this).scrollTop();
          var m = 350;
          if(s > m) {
             $(".faq-float").fadeIn(500);
          } else if(s < m) {
             $(".faq-float").fadeOut("fast");
          }
       });

    }
});
