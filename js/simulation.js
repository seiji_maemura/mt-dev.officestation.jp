
function feecalculation(){

  //フォームの取得
  var simulation_frm = document.simulation_form;
  var submit_frm = document.submit_form;

  //要素の取得
  var error = document.getElementById("error");
  var usage_period = document.getElementById('usage_period');
  var result_yearly = document.getElementById("result_yearly");
  var result_monthly = document.getElementById("result_monthly");
  var result_monthly_perperson = document.getElementById("result_monthly_perperson");

  //受け取った各値を入れる変数
  var service = [];
  var labor = "";
  var year_end_ajustment = "";
  var payslip = "";
  var paid_holiday_management = "";
  var my_number = "";
  var call_center_option = "";
  var users = "";

  //処理開始パス
  var start_simulation = "0";

  //表示のリセット
  result_yearly.innerHTML = "0 円";
  result_monthly.innerHTML = "0 円";
  result_monthly_perperson.innerHTML = "0 円";
  error.innerHTML = "";

  //「全て」にチェックが入っている場合の処理
  if(simulation_frm.all_service.checked){
    for(i = 0; i < simulation_frm.service.length; i++){
         simulation_frm.service[i].checked = true;
     }
  }

  //チェックリストに入力された値を取得
    for(i = 0; i < simulation_frm.service.length; i++){
       if(simulation_frm.service[i].checked){
         service[i] = simulation_frm.service[i].value;
       }else{
         service[i] = 0;
       }
     }

  labor =  service[0];
  year_end_ajustment =  service[1];
  payslip = service[2];
  paid_holiday_management =  service[3];
  my_number =  service[4];

  if(simulation_frm.call_center_option.checked){
    call_center_option = simulation_frm.call_center_option.value;
  }else {
    call_center_option = 0;
  }

  // 送信フォームへ値の引継ぎ
    if (labor != 0) {
      submit_frm.result_service[0].checked = true;
    }else {
      submit_frm.result_service[0].checked = false;
    }
    if (year_end_ajustment != 0) {
      submit_frm.result_service[1].checked = true;
    }else {
      submit_frm.result_service[1].checked = false;
    }
    if (payslip != 0) {
      submit_frm.result_service[2].checked = true;
    }else {
      submit_frm.result_service[2].checked = false;
    }
    if (paid_holiday_management != 0) {
      submit_frm.result_service[3].checked = true;
    }else {
      submit_frm.result_service[3].checked = false;
    }
    if (my_number != 0) {
      submit_frm.result_service[4].checked = true;
    }else {
      submit_frm.result_service[4].checked = false;
    }
    if (call_center_option != 0) {
      submit_frm.result_service[5].checked = true;
    }else {
      submit_frm.result_service[5].checked = false;
    }



  //テキストボックスの値を全角→半角へ変換
  users = simulation_frm.users.value;
  users = users.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function(s) {
    return String.fromCharCode(s.charCodeAt(0) - 65248);
  });


  //利用人数の判定
  if (users == "" ) {
  error.innerHTML = "利用人数が入力されていません";
  start_simulation = "0";
  usage_period.options[1].selected = false;
  }else if (users.match(/^[0-9]+$/)) {
    users = parseInt( users, 10);
    error.innerHTML = "";
    if(users == 0){
      error.innerHTML = "利用人数が入力されていません";
      start_simulation = "0";
      usage_period.options[1].selected = false;
    }else if( users > 30000 ) {
      start_simulation = "0";
      usage_period.options[1].selected = false;
      alert("利用人数が30000人を超える場合シミュレーションの対象外になります。");
    }else {
      start_simulation = "1";
      usage_period.options[1].selected = true;
    }
    // 利用人数をフォームに同期
    // $("#users_number").val(users);
  }else{
    error.innerHTML = "正しい値もしくは形式で入力してください。";
    start_simulation = "0";
    usage_period.options[1].selected = false;
  }


  //シミュレーションの本処理
  if(start_simulation == "1"){

    //料金表から取得した料金を入れる変数
    var labor_y = "";
    var mynumber_op_y = "";
    var mynumber_y = "";
    var year_y = "";
    var pay_y = "";
    var holiday_y = "";
    var call_y = "";
    var inquiry_result = "";

    //月額料金
    var labor_m = 0;
    var year_m = 0;
    var pay_m = 0;
    var holiday_m = 0;
    var mynumber_m =  0;
    var labor_mynumber_m = 0;
    var call_m = 0;

    //合計料金
    var labor_mynumber_y = 0;
    var result_m = 0;
    var result_y = 0;
    var result_m_p = 0;

    //料金照会→取得
    inquiry_result = price(users);
    labor_y = inquiry_result["オフィスステーション労務（年額）"];
    mynumber_op_y = inquiry_result["労務マイナンバーオプション（年額）"];
    mynumber_y = inquiry_result["マイナンバー（労務が選択されていない場合）（年額）"];
    year_y = inquiry_result["年末調整（年額）"];
    pay_y = inquiry_result["給与明細（年額）"];
    holiday_y = inquiry_result["有休管理（年額）"];
    call_y = inquiry_result["コールセンターOP（年額）"];

    //条件別シミュレーション

      if (labor != 0 && my_number != 0) {
        //労務+マイナンバー
        labor_mynumber_y = labor_y + mynumber_op_y;
        labor_y = 0;
        mynumber_y = 0;
      }else {

        if (labor == 0) {
          labor_y = labor;
        }

        if (my_number == 0) {
          mynumber_y = my_number;
        }

      }

      if (year_end_ajustment == 0) {
        year_y = year_end_ajustment;
      }

      if (payslip == 0) {
        pay_y = payslip;
      }

      if (paid_holiday_management == 0) {
        holiday_y = paid_holiday_management;
      }

      //共通処理
      if (call_center_option == 0) {
        call_y = call_center_option;
      }

      //最終計算(小数点以下四捨五入)
      result_y = labor_y + year_y + pay_y + holiday_y + mynumber_y + labor_mynumber_y + call_y;

      labor_m = labor_y/11;
      year_m = year_y/12;
      pay_m = pay_y/12;
      holiday_m = holiday_y/12;
      mynumber_m =  mynumber_y/12;
      labor_mynumber_m = labor_mynumber_y/11;
      call_m = call_y/12;
      labor_m = Math.round(labor_m);
      year_m = Math.round(year_m);
      pay_m = Math.round(pay_m);
      holiday_m = Math.round(holiday_m);
      mynumber_m =  Math.round(mynumber_m);
      labor_mynumber_m = Math.round(labor_mynumber_m);
      call_m = Math.round(call_m);
      result_m = labor_m + year_m + pay_m + holiday_m + mynumber_m + labor_mynumber_m + call_m;

      result_m_p = (result_m)/users;
      result_m_p = Math.round(result_m_p);

      //カンマ付け
      result_y = Number(result_y).toLocaleString();
      result_m = Number(result_m).toLocaleString();
      result_m_p = Number(result_m_p).toLocaleString();

      //結果の出力
      result_yearly.innerHTML = result_y + " 円";
      result_monthly.innerHTML = result_m + " 円";
      result_monthly_perperson.innerHTML = result_m_p + " 円";

  }//シミュレーションの本処理ここまで
}
$(function(){
  // 業種選択時 従業員数へ値引継ぎ
  $('#business-category').change(function() {
    const check = $("#business-category").val();
    if (check == '一般企業') {
      var target = $('#users').val();
      $("#number-of-employees").val(target);
    }
  });
});