$(function() {
    $(".products-tab li a").click(function(){
        $(".products-tab li").removeClass("active");
        $(this).parent(".products-tab li").addClass("active");
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        if( href == "#products_all") {
            $(".products-body").show();
            return false;
        } else {
            $(".products-body").hide();
            $(target).show();
            return false;
        }
    });
    $(".products-switch li a").click(function(){
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        $(this).parent(".products-switch li").toggleClass("active");
        $(target).toggleClass("active");
        if ($(this).parents(".products-switch").find("li").hasClass("active")) {
            $(this).parents(".products-body").find(".products-body-block-none").hide();
        } else {
            $(this).parents(".products-body").find(".products-body-block-none").show();
        }
        return false;
    });

    $(".products-table .sp_ac th").click(function(){
        $(this).next().find("div").slideToggle(300);
    });
    $(window).on('load resize', function(){
        var winW = $(window).width();
        var devW = 767;
        if (winW >= devW) {
            $(".products-table .sp_ac td div").show();
        } else {
            $(".products-table .sp_ac td div").hide();
        }
    });

   	$('#header-menu, #header-menu-f').click(function() {
      	$("#g-nav-r").fadeIn("fast");
   	});
   	$('#g-nav-close').click(function() {
      	$("#g-nav-r").hide();
   	});

    // ハンバーガーメニュー内製品、9つの神対応次階層メニュークリック挙動
    $('#gl1').click(function() {
        $(this).find('.g-nav-subcontent').toggle('fast');
        if($('#gl1').find('.g-nav-subcontent').css('display')=='block'){
          if($('#gl3').find('.g-nav-subcontent').css('display')=='block'){
            $('#gl3 .g-nav-subcontent').toggle('fast');
          }
        }
      });
      $('#gl3').click(function() {
        $(this).find('.g-nav-subcontent').toggle('fast');
        if($('#gl1').find('.g-nav-subcontent').css('display')=='block'){
          if($('#gl3').find('.g-nav-subcontent').css('display')=='block'){
            $('#gl1 .g-nav-subcontent').toggle('fast');
          }
        }
      });
});

$(function(){
   $('#feature-pop01').click(function() {
   		$("#feature-block01").fadeIn("fast");
   });
   $('.feature-block-close').click(function() {
   		$(".feature-block").hide();
   });
});
$(function(){
   $('#feature-pop02').click(function() {
   		$("#feature-block02").fadeIn("fast");
   });
   $('.feature-block-close').click(function() {
   		$(".feature-block").hide();
   });
});

$(function () {
    $('#simu-pop').click(function () {
        $("#simu-pop-block").fadeIn("fast");
    });
    $('#simu-pop-close').click(function () {
        $("#simu-pop-block").hide();
    });
});


$(window).on('load resize', function(){
    var winW2 = $(window).width();
    var devW2 = 960;
    if (winW2 >= devW2) {
        $(window).scroll(function () {
            var s = $(this).scrollTop();
            var m = 300;
            if(s > m) {
                $("#header-f").fadeIn('400');
                $("#f-footer").fadeIn('400');
            } else if(s < m) {
                $("#header-f").fadeOut('200');
                $("#f-footer").fadeOut('200');
            }
        });
        //$("#header-f").show();
    } else {
        $("#header-f").hide();
        $("#f-footer").hide();
    }
});