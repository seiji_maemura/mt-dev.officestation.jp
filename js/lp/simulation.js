document.addEventListener("DOMContentLoaded", function () {
  $(function () {
    var option = '<option value="#value">#name</option>';
    var main = {
      "main_count": {
        "persons": [50, 100, 200, 300, 400, 500],
        "places": [0, 1, 2, 3, 4, 5, 6],
        "web_calc": 600,
        "web_calc2": 480
      },
      "sample": {
        "persons": [50, 500, 1.5, 5750, 1000],
        "places": [1, 400, 2300]
      }
    };
    var options = "";
    main["main_count"]["persons"].forEach(function (element) {
      var op = option.replace("#value", element).replace("#name", element);
      options += op;
    });
    $(".people-select").html(options);
    options = "";
    main["main_count"]["places"].forEach(function (element) {
      var op = option.replace("#value", element).replace("#name", element);
      options += op;
    });
    $("#simulation .place-select").html(options);

    function separate(num) {
      return String(num).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
    }

    function calculate() {
      var people = $(".people-select").val();
      var place = $(".place-select").val();
      var place2 = parseInt($(".place-select").val()) + 1;
      var r = people / main.sample.persons[0];
      // var r = 100 / 50;
      var r2 = place / main.sample.places[0];
      // var r2 = 1 / 1;
      var r3 = place2 / main.sample.places[0];
      var print = ((main.sample.persons[1] * r) * 12) / 1000;
      // var print = 500 * 1/5; 1000
      var cover = ((main.sample.persons[4] * r) * 12) / 1000;
      // var cover = main.sample.persons[1] * 2; 2000
      var sort = ((main.sample.persons[3] * r) * 12) / 1000;
      // var sort = 5750 * 2; 130000, 11500
      var mail = ((main.sample.places[1] * r2) * 12) / 1000;
      // var mail = 1 * r2; 1000, 400
      var distribution = ((main.sample.places[2] * r3) * 12) / 1000;

      var print2 = ((main.sample.persons[1] * r) * 12);
      var cover2 = ((main.sample.persons[4] * r) * 12);
      var sort2 = ((main.sample.persons[3] * r) * 12);
      var mail2 = ((main.sample.places[1] * r2) * 12);
      var distribution2 = ((main.sample.places[2] * r3) * 12);

      var sum = ((print2 + cover2 + sort2 + mail2 + distribution2) / 1000);
      var sub_sum = ((people * main.main_count.web_calc) / 1000);
      if (people == 500) var sub_sum = ((people * main.main_count.web_calc2) / 1000);
      var sum2 = (print2 + cover2 + sort2 + mail2 + distribution2);

      var sub_sum2 = (people * main.main_count.web_calc);
      if (people == 500) var sub_sum2 = (people * main.main_count.web_calc2);

      var difference2 = (sum2 - sub_sum2);
      // var difference2 = difference ;

      $("#simulation .print").text(separate(print));
      $("#simulation .cover").text(separate(cover));
      $("#simulation .sort").text(separate(sort));
      $("#simulation .mail").text(separate(mail));
      $("#simulation .distribution").text(separate(distribution));
      $("#simulation .total_main").text(separate(sum));
      $("#simulation .total_sub").text(separate(sub_sum));
      $("#simulation .month_web").text(separate(main.main_count.web_calc));
      if (people == 500) $("#simulation .month_web").text(separate(main.main_count.web_calc2));
      $("#simulation .difference").text(separate(difference2));
    }

    $("#simulation .people-select,#simulation .place-select").change(calculate);
    calculate();
  });
});