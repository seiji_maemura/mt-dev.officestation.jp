<?php
    include '../../../module/form/main.php';
?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
    <meta name="format-detection" content="telephone=no">
    <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
    <meta name="robots" content="noindex,nofollow">
    <title>年末調整セミナーお申込みありがとうございます。</title>
    <script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>

    <link rel="canonical" href="https://www.officestation.jp/seminar/form-nencho/form-thanks.php" />
    <meta property="og:title" content="年末調整セミナーお申込みありがとうございます。" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://www.officestation.jp/seminar/form-nencho/form.php" />
    <meta property="og:image" content="https://www.officestation.jp/nencho/lp/src/img/ogimage.jpg" />
    <meta property="og:site_name" content="クラウド型労務・人事管理システム「オフィスステーション」" />
    <meta property="og:image:secure_url" content="https://www.officestation.jp/nencho/lp/src/img/ogimage.jpg" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="年末調整セミナーお申込みありがとうございます。" />
    <meta name="twitter:image" content="https://www.officestation.jp/nencho/lp/src/img/ogimage.jpg" />

    <link rel='dns-prefetch' href='//s.w.org' />
    <!-- Markup (JSON-LD) structured in schema.org ver.4.6.5 START -->
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "BreadcrumbList",
        "itemListElement": [
            {
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "https://www.officestation.jp",
                    "name": "オフィスステーション"
                }
            },
            {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "https://www.officestation.jp/seminar/",
                    "name": "セミナー一覧"
                }
            }
            {
                "@type": "ListItem",
                "position": 3,
                "item": {
                    "@id": "https://www.officestation.jp/seminar/form-nencho/form.php",
                    "name": "年末調整セミナーお申し込み"
                }
            }
        ]
    }
    </script>
    <!-- Markup (JSON-LD) structured in schema.org END -->

    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/styles_sp.css">
    <link rel="stylesheet" href="/css/styles_.css">
    <link rel="stylesheet" href="/css/styles-t.css">
    <link rel="stylesheet" href="/css/styles_sp-t.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/common_sp.css">
    <link rel="stylesheet" href="/css/styles_i.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="./dist/style.css">

    <!-- JS Setting-->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery.matchHeight.js"></script>
    <script src="/js/scripts.js"></script>

    <!-- Google Analyticsへのデータ連携 -->
    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({'customer': '2', 'employees': '<?php if( $clean['number_of_employees'] != "" ) { echo $clean['number_of_employees']; } ?>', 'capital': '<?php if( !empty($clean['company_capital']) ) { echo $clean['company_capital']; } ?>', 'ads': '<?php if( !empty($clean['inflow_source']) ) { echo $clean['inflow_source']; } ?>'});
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NS5BM5J');</script>
    <!-- End Google Tag Manager -->

</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

    <div id="frame-outer">
    <header class="l-header u-reset__header">
        <div class="l-container u-pb0">
            <div class="l-header__logo">
                <img src="./src/img/logo-nencho.png"
                     alt="オフィスステーション 年末調整"
                >
            </div>
        </div>
    </header>
    <div class="content">
        <div class="page-ttl">
            <h1>
                <span>セミナーお申込み</span>
            </h1>
        </div>
        <div id="bread">
            <ul>
                <li><a href="/">HOME</a></li>
                <li><a href="/seminar/">セミナー一覧</a></li>
                <li><a href="/seminar/form.php">セミナーお申込み</a></li>
            </ul>
        </div>
        <section class="block">
            <div class="section-inner">
                <div class="wrap">
                    <div class="c-ttl2 m-t30 m-b80 sp_m-b40">
                        <div class="main">
                            <h2>セミナーお申込みを承りました。</h2>
                        </div>
                    </div>
                    <div class="c-txt m-b40">
                        <p><span class="dis-ib t-l">このたびは、セミナーにお申込みいただき、誠にありがとうございます。<br><br>
                        差出人「ウェビナー情報」、件名「ウェビナー参加申込完了」のメールにてご連絡いたしますので、今しばらくお待ちください。<br>
                        ウェビナー参加時は、そのメール本文のリンク先よりご参加ください。<br><br>
                        今後とも「オフィスステーション」をよろしくお願い申し上げます。</span><p>
                    </div>
                    <!-- Pardotへのデータ連携 -->
                    <?php if( !empty($_POST) && !empty($clean) ) {
                        $last_name = urlencode($clean['last_name']);
                        $first_name = urlencode($clean['first_name']);
                        $user_company = urlencode($clean['user_company']);
                        $company_capital = urlencode($clean['company_capital']);
                        $number_of_employees = urlencode($clean['number_of_employees']);
                        $user_mail = urlencode($clean['user_mail']);
                        $user_tel = urlencode($clean['user_tel']);
                        $seminar = urlencode($clean['seminar']);
                        $inflow_source = urlencode($clean['inflow_source']);
                        $agree = urlencode($clean['agree']);

                        echo '<iframe id="iframe" src="https://go.officestation.jp/l/723363/2020-07-07/bqrzn?last_name='.$last_name.'&first_name='.$first_name.'&user_company='.$user_company.'&company_capital='.$company_capital.'&number_of_employees='.$number_of_employees.'&user_mail='.$user_mail.'&user_tel='.$user_tel.'&seminar='.$seminar.'&inflow_source='.$inflow_source.'&agree='.$agree.'" width="1" height="1"></iframe>';
                    }
                    ?>
                </div>
            </div>
        </section>
    </div>

    <footer class="l-footer u-reset__footer">
        <div class="l-container l-container__flex u-pb0">
            <div class="l-footer__logo">
                <img src="/img/common/c_logo.png" alt="株式会社エフアンドエム">
            </div>
            <p class="l-footer__copy-right">&copy; 2020 F&M co.,ltd.</p>
        </div>
    </footer>
    <script>
        $(function(){
            $('a[href^="#"]').click(function(){
            var speed = 500;
            var href= $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            //ヘッダーの高さを取得
            var header = $('header').height();
            //ヘッダーの高さを引く
            var position = target.offset().top - header;
            $("html, body").animate({scrollTop:position}, speed, "swing");
            return false;
            });
        });
    </script>
    <script src="./dist/main.js"></script>
</body>
</html>
