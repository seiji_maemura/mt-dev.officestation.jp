<MTSetVarBlock name="set_ttl">
    <mt:If tag="TemplateDataTitle">
        <mt:TemplateDataTitle>
    </mt:If>
</MTSetVarBlock>
<MTSetVarBlock name="set_description">
    <mt:If tag="TemplateDataMetadescription">
        <mt:TemplateDataMetadescription>
    </mt:If>
</MTSetVarBlock>
<MTSetVarBlock name="set_keywords">
    <mt:If tag="TemplateDataMetakeywords">
        <mt:TemplateDataMetakeywords>
    </mt:If>
</MTSetVarBlock>
<MTSetVarBlock name="set_canonical">
    <mt:If tag="TemplateDataCanonical">
        <mt:TemplateDataCanonical>
    </mt:If>
</MTSetVarBlock>
<MTIgnore>
    <MTSetVarBlock name="set_ogimage">
        <mt:If tag="TemplateDataOgimage">
            <mt:TemplateDataOgimage>
        </mt:If>
    </MTSetVarBlock>
</MTIgnore>
<$MTInclude module="_ini" parent="1" $>
    <$MTSetVar name="lpName" value="seminar-a01" $>
        <mt:Unless name="compress" regex_replace="/^\s*\n/gm" ,"">
            <MTIgnore>不要なCMSからの空白改行を除去</MTIgnore>
            <?php
            include '../../../module/form/main.php';
            include '../../../module/form/seminar-nencho.php';
            ?>
            <!DOCTYPE html>
            <html lang="ja">

            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
                <meta name="format-detection" content="telephone=no">
                <link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
                <link rel="shortcut icon" type="image/vnd.microsoft.icon" href="/favicon.ico">
                <title>
                    <$MTVar name="set_ttl" $>
                </title>
                <link rel="canonical" href="<$MTVar name=" set_canonical"$>" />
                <MTIgnore>
                    <meta name="keywords" content="<MTVar name=" set_keywords">" /></MTIgnore>
                <meta name="description" content="<$MTVar name=" set_description"$>" />
                <meta property="og:title" content="<$MTVar name=" set_ttl"$>" />
                <meta property="og:type" content="<MTIf name=" body" eq="home">website<MTElse>article</MTIf>" />
                    <meta property="og:url" content="" />
                    <meta property="og:site_name" content="<$MTVar name=" baseSiteName"$>" />
                    <meta property="og:image" content="https://www.officestation.jp/nencho/lp/a01/src/img/ogp.png" />
                    <meta property="og:image:secure_url" content="https://www.officestation.jp/nencho/lp/a01/src/img/ogp.png" />
                    <meta name="twitter:card" content="summary" />
                    <meta name="twitter:title" content="<$MTVar name=" set_ttl"$>" />
                    <meta name="twitter:image" content="https://www.officestation.jp/nencho/lp/a01/src/img/ogp.png" />

                    <!-- clear a cache -->
                    <meta http-equiv="Pragma" content="no-cache">
                    <meta http-equiv="Cache-Control" content="no-cache">
                    <meta http-equiv="Expires" content="0">
                    <link rel="stylesheet" href="/module/form/form.css">
                    <link rel="stylesheet" href="./dist/style.css">
                    <link rel='dns-prefetch' href='//s.w.org' />
                    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                    <link href="//fonts.googleapis.com/css?family=Noto+Sans+JP:400,900|Noto+Serif+JP|Roboto+Condensed:700&display=swap" rel="stylesheet">

                    <!-- JS Setting-->
                    <script src="//code.jquery.com/jquery-3.4.1.min.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
                    <script src='https://www.google.com/recaptcha/api.js'></script>

                    <!-- Google Tag Manager -->
                    <script>
                        (function(w, d, s, l, i) {
                            w[l] = w[l] || [];
                            w[l].push({
                                'gtm.start': new Date().getTime(),
                                event: 'gtm.js'
                            });
                            var f = d.getElementsByTagName(s)[0],
                                j = d.createElement(s),
                                dl = l != 'dataLayer' ? '&l=' + l : '';
                            j.async = true;
                            j.src =
                                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                            f.parentNode.insertBefore(j, f);
                        })(window, document, 'script', 'dataLayer', 'GTM-NS5BM5J');
                    </script>
                    <!-- End Google Tag Manager -->

                    <!-- User Heat Tag -->
                    <script type="text/javascript">
                        (function(add, cla) {
                            window['UserHeatTag'] = cla;
                            window[cla] = window[cla] || function() {
                                (window[cla].q = window[cla].q || []).push(arguments)
                            }, window[cla].l = 1 * new Date();
                            var ul = document.createElement('script');
                            var tag = document.getElementsByTagName('script')[0];
                            ul.async = 1;
                            ul.src = add;
                            tag.parentNode.insertBefore(ul, tag);
                        })('//uh.nakanohito.jp/uhj2/uh.js', '_uhtracker');
                        _uhtracker({
                            id: 'uhIcAz08lS'
                        });
                    </script>
                    <!-- End User Heat Tag -->

            </head>

            <body id="top" class="seminar-nencho">

                <!-- Google Tag Manager (noscript) -->
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NS5BM5J" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <!-- End Google Tag Manager (noscript) -->

                <div id="fb-root"></div>
                <script>
                    (function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s);
                        js.id = id;
                        js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.10';
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>

                <header class="l-header u-dpf">
                    <div class="l-container u-pb0">
                        <div class="l-header__logo">
                            <img src="./src/img/logo-nencho.png" alt="オフィスステーション 年末調整">
                        </div>
                    </div>
                </header>

                <?php if ($page_flag === 0) : ?>

                    <?php if (empty($_POST['btn_confirm'])) : ?>
                        <section class="l-container">
                            <div class="c-mv">
                                <h1>
                                    <img id="img-mv-pc" src="./src/img/img-mv.png" alt="2020年こそWebでスイスイスマートに！ペーパーレス年末調整入門講座 | オンライン開催">
                                </h1>
                                <div class="c-mv__btn-wrapper">
                                    <a class="c-btn" href="#form-bottom">
                                        お申込みはコチラ
                                    </a>
                                </div>
                            </div>
                        </section>

                        <section class="l-container">
                            <h2 class="l-container__title">
                                講座概要
                                <span class="l-container__sub-title">
                                    About
                                </span>
                            </h2>
                            <div class="p-about">
                                <p class="p-about__paragraph">
                                    2020年はHR業界に大きな影響を与える制度改正がめじろ押しでした。</p>
                                <p class="p-about__paragraph">4月にスタートした労務手続きの電子申請義務化にはじまり、年末調整に大きな影響を及ぼす基礎控除の改正など大きな税制改正もありました。</p>
                                <p class="p-about__paragraph">
                                    そこに加えて新型コロナウィルスの登場は私たちの仕事や生活を一変させてしまいました。しかし一方でこれほどまでに“働き方”を考えさせられたという側面も無視できません。</p>
                                <p><strong class="p-about__main-msg">本セミナーでは、2020年に新たに発生した制度改正を振り返るとともに、コロナ禍に代表される有事にも対応すべく新時代のバックオフィス業務の最適解をご提案します。</strong></p>
                            </div>

                            <div class="c-panel">
                                <div class="c-panel__head">
                                    <h2 class="c-panel__title u-ornament">
                                        こういう方にオススメ
                                    </h2>
                                </div>
                                <div class="c-panel__body">
                                    <ul class="p-recommended">
                                        <li class="p-recommended__item">
                                            会社のテレワーク化を進めたい方
                                        </li>
                                        <li class="p-recommended__item">
                                            従業員からの情報収集をペーパーレス化したい方
                                        </li>
                                        <li class="p-recommended__item">
                                            人事総務関連の業務で紙をとにかくなくしたい
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </section>

                        <section class="l-container">
                            <h2 class="l-container__title">
                                講座内容
                                <span class="l-container__sub-title">
                                    Content
                                </span>
                            </h2>
                            <div class="p-content">
                                <ul class="p-content__items">
                                    <li class="p-content__item">
                                        コロナ禍におけるIT化の推進の現状と課題
                                    </li>
                                    <li class="p-content__item">
                                        総務人事がテレワークを進める上でのあるある
                                    </li>
                                    <li class="p-content__item">
                                        ペーパーレスというけど具体的になにがどうなるの？
                                    </li>
                                    <li class="p-content__item">
                                        「オフィスステーション」のご紹介
                                    </li>
                                    <li class="p-content__item">
                                        導入企業さまのご活用事例
                                    </li>
                                </ul>
                                <div class="p-content__img">
                                    <img src="./src/img/img-content.png" alt="講座内容のイメージ画像">
                                </div>
                            </div>
                        </section>

                        <section class="l-container">
                            <h2 class="l-container__title">
                                講師紹介
                                <span class="l-container__sub-title">
                                    Speaker
                                </span>
                            </h2>
                            <div class="p-speaker">
                                <div class="p-speaker__img">
                                    <img src="./src/img/img_Higashino.png" alt="講師紹介イメージ画像">
                                </div>
                                <div class="p-speaker__text-wrapper">
                                    <h3 class="p-speaker__profile">
                                        <span class="p-speaker__company">
                                            株式会社エフアンドエム
                                        </span>
                                        <span class="p-speaker__dept">
                                            オフィスステーション事業本部 インサイドセールス推進事業部
                                        </span>
                                        <span class="p-speaker__name">
                                            東野 光宏
                                        </span>
                                    </h3>
                                    <p class="p-speaker__description">
                                        福岡県出身。個人事業主向け、法人企業向け、士業向けに様々なテーマでセミナーを企画し、累計1,000回以上の講演を実施。実務者目線の情報を軽快なテンポでわかりやすく伝えることが得意。オフィスステーション事業には立ち上げから関わっており、大企業から少数精鋭の企業まで数百社のバックオフィスの効率化支援を「オフィスステーション」を通じて経験。現在は、オフィスステーション事業のインサイドセールス部門の責任者として活動中。
                                    </p>
                                </div>
                            </div>
                            <div class="p-speaker">
                                <div class="p-speaker__img">
                                    <img src="./src/img/img_Ike.png" alt="講師紹介イメージ画像">
                                </div>
                                <div class="p-speaker__text-wrapper">
                                    <h3 class="p-speaker__profile">
                                        <span class="p-speaker__company">
                                            株式会社エフアンドエム
                                        </span>
                                        <span class="p-speaker__dept">
                                            オフィスステーション事業本部 企業ソリューション推進 第2事業部
                                        </span>
                                        <span class="p-speaker__name">
                                            池邉 俊貴　
                                        </span>
                                    </h3>
                                    <p class="p-speaker__description">
                                        入社16年目。これまでエフアンドエムでルートセールス、マネジメント、新規販路開拓の責任者などを歴任。そこで新たな収益のモデルを生み出すなどを経験。
                                        これまで現場で出会った3,000人を超える経営者、社労士、企業担当者から得た「改善例」を基にしたセミナーを年間70～100回開催。人事総務業界ならではの「今」を日々発信している。
                                    </p>
                                </div>
                            </div>
                        </section>

                        <section class="l-container__fluid u-bg-custom">
                            <div class="l-container">
                                <h2 class="l-container__title">
                                    開催概要
                                    <span class="l-container__sub-title">
                                        Overview
                                    </span>
                                </h2>
                                <div class="c-panel">
                                    <div class="c-panel__body">
                                        <table class="p-overview">
                                            <tbody>
                                                <tr class="p-overview__row">
                                                    <th class="p-overview__heading">
                                                        <span class="p-overview__heading__text">料金</span>：
                                                    </th>
                                                    <td class="p-overview__data">
                                                        無料
                                                    </td>
                                                </tr>
                                                <tr class="p-overview__row">
                                                    <th class="p-overview__heading">
                                                        <span class="p-overview__heading__text">対象</span>：
                                                    </th>
                                                    <td class="p-overview__data">
                                                        総務・人事部の雇用・社会保険手続きご担当者様
                                                    </td>
                                                </tr>
                                                <tr class="p-overview__row">
                                                    <th class="p-overview__heading">
                                                        <span class="p-overview__heading__text">場所</span>：
                                                    </th>
                                                    <td class="p-overview__data">
                                                        オンライン（Zoom）
                                                    </td>
                                                </tr>
                                                <tr class="p-overview__row">
                                                    <th class="p-overview__heading">
                                                        <span class="p-overview__heading__text">主催</span>：
                                                    </th>
                                                    <td class="p-overview__data">
                                                        株式会社エフアンドエム
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="l-container__fluid u-bg-sub-2">
                            <div class="l-container">
                                <h2 class="l-container__title u-ornament u-ornament__bg-color">開催日一覧</h2>
                                <MTSetVarBlock name="object">企業向け</MTSetVarBlock>
                                <MTIgnore>
                                    各テンプレートで対象としたセミナーのみ表示 グローバルTOPなら全て、各LPは企業用のみ、PROは士業用のみ
                                </MTIgnore>
                                <$MTInclude module="セミナー情報"$>
                            </div>
                        </section>

                        <section id="form-bottom" class="l-container__fluid u-bg-sub-2">
                            <div class="l-container">
                                <h2 class="l-container__title u-ornament">お申込みはコチラ</h2>
                                <form action="" method="post" class="form c-form">
                                    <?php if (!empty($error)) : ?>
                                        <ul class="error-list">
                                            <?php foreach ($error as $value) : ?>
                                                <li><?php echo $value; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                    <!-- 氏名記入 -->
                                    <div class="form-heading">
                                        <label class="required">氏名</label>
                                        <div class="answer-box">
                                            <div class="name-box">
                                                <div class="input-wrapper">
                                                    <input class="form-control is_input" type="text" id="name" name="last_name" placeholder="姓" value="<?php if (!empty($clean['last_name'])) {
                                                                                                                                                            echo $clean['last_name'];
                                                                                                                                                        } ?>" required>
                                                </div>
                                                <div class="input-wrapper">
                                                    <input class="form-control is_input" type="text" id="name" name="first_name" placeholder="名" value="<?php if (!empty($clean['first_name'])) {
                                                                                                                                                            echo $clean['first_name'];
                                                                                                                                                        } ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 会社名記入 -->
                                    <div class="form-heading">
                                        <label class="required">会社名</label>
                                        <div class="answer-box">
                                            <input class="form-control is_input" type="text" id="company-name" name="user_company" placeholder="例：株式会社○○" value="<?php if (!empty($clean['user_company'])) {
                                                                                                                                                                        echo $clean['user_company'];
                                                                                                                                                                    } ?>" required>
                                        </div>
                                    </div>
                                    <!-- 資本金記入 -->
                                    <div class="form-heading">
                                        <label class="required">資本金</label>
                                        <div class="answer-box">
                                            <select name="company_capital" id="company-capital" required>
                                                <?php if (!empty($company_capitals)) : ?>
                                                    <?php foreach ($company_capitals as $company_capital) : ?>
                                                        <option value="<?php echo $company_capital; ?>" <?php if (!empty($clean['company_capital']) && $clean['company_capital'] === $company_capital) {
                                                                                                            echo 'selected';
                                                                                                        } ?>><?php echo $company_capital; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!--従業員数記入 -->
                                    <div class="form-heading">
                                        <label class="required">従業員数</label>
                                        <div class="answer-box">
                                            <input class="form-control" type="text" id="number-of-employees" class="is_input" name="number_of_employees" placeholder="例：100" value="<?php if (!empty($clean['number_of_employees'])) {
                                                                                                                                                                                        echo $clean['number_of_employees'];
                                                                                                                                                                                    } ?>" required>
                                        </div>
                                    </div>
                                    <!-- メールアドレス記入 -->
                                    <div class="form-heading">
                                        <label class="required" for="mail">メールアドレス</label>
                                        <div class="answer-box">
                                            <input class="form-control is_input" type="text" id="mail" name="user_mail" placeholder="例：example@officestation.jp" value="<?php if (!empty($clean['user_mail'])) {
                                                                                                                                                                            echo $clean['user_mail'];
                                                                                                                                                                        } ?>" required>
                                        </div>
                                    </div>
                                    <!-- 電話番号記入 -->
                                    <div class="form-heading">
                                        <label class="required" for="tel">電話番号</label>
                                        <div class="answer-box">
                                            <input class="form-control" type="text" id="tel" name="user_tel" placeholder="例：000-000-0000" value="<?php if (!empty($clean['user_tel'])) {
                                                                                                                                                        echo $clean['user_tel'];
                                                                                                                                                    } ?>" required>
                                        </div>
                                    </div>
                                    <!-- 参加予定のセミナー記入 -->
                                    <div class="form-heading">
                                        <label class="required">参加予定のセミナー</label>
                                        <div class="answer-box">
                                            <select name="seminar" required>
                                                <option value=""></option>
                                                <?php if (!empty($seminars)) : ?>
                                                    <?php foreach ($seminars as $seminar) : ?>
                                                        <option value="<?php echo $seminar; ?>" <?php if (!empty($clean['seminar']) && $clean['seminar'] === $seminar) {
                                                                                                    echo 'selected';
                                                                                                } ?>><?php echo $seminar; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- 広告アンケート記入 -->
                                    <div class="form-heading inflow_source">
                                        <label>「オフィスステーション 年末調整」を知った理由</label>
                                        <div class="answer-box">
                                            <?php if (!empty($inflow_sources)) : ?>
                                                <?php foreach ($inflow_sources as $id => $value) : ?>
                                                    <label for="<?php echo $id; ?>"><input id="<?php echo $id; ?>" type="radio" name="inflow_source" value="<?php echo $value; ?>" <?php if (!empty($clean['inflow_source']) && $clean['inflow_source'] === $value) {
                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                    } ?>><?php echo $value; ?></label>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <input type="text" name="othertext" id="othertext" value="<?php if (!empty($clean['othertext'])) {
                                                                                                            echo $clean['othertext'];
                                                                                                        } ?>">
                                        </div>
                                    </div>
                                    <!-- プライバシー同意 -->
                                    <div class="form-heading">
                                        <label class="required">プライバシーステートメントを確認し、同意します。</label>
                                        <div class="answer-box">
                                            <span class="value">
                                                <input type="checkbox" name="agree" id="agree" value="同意する" <?php if ($clean['agree'] === "同意する") {
                                                                                                                echo 'checked';
                                                                                                            } ?> required>
                                                <label class="agree" for="agree">同意する</label>
                                            </span>
                                            <p class="description"><a href="https://www.officestation.jp/privacy-statement/" target="_blank">プライバシーステートメント</a></p>
                                        </div>
                                    </div>
                                    <!-- バリデーションチェック用 -->
                                    <input type="hidden" name="agree_check">

                                    <!-- 私はロボットではありません -->
                                    <div class="g-recaptcha" data-callback="clearcall" data-sitekey="<?php echo $site_key ?>"></div>
                                    <!-- 確認画面へボタン -->
                                    <p class="submit text-center">
                                        <input id="os-submit" type="submit" name="btn_confirm" class="button" value="確認画面へ" disabled>
                                    </p>
                                </form>
                            </div>
                        </section>

                    <?php else : ?>

                        <section id="form-bottom" class="l-container__fluid">
                            <div class="l-container">
                                <h2 class="l-container__title">お申込みはコチラ</h2>
                                <form action="" method="post" class="form c-form">

                                    <?php if (!empty($error)) : ?>
                                        <ul class="error-list">
                                            <?php foreach ($error as $value) : ?>
                                                <li><?php echo $value; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>

                                    <!-- 氏名記入 -->
                                    <div class="form-heading">
                                        <label class="required">氏名</label>
                                        <div class="answer-box">
                                            <div class="name-box">
                                                <div class="input-wrapper">
                                                    <input class="form-control is_input" type="text" id="name" name="last_name" placeholder="姓" value="<?php if (!empty($clean['last_name'])) {
                                                                                                                                                            echo $clean['last_name'];
                                                                                                                                                        } ?>" required>
                                                </div>
                                                <div class="input-wrapper">
                                                    <input class="form-control is_input" type="text" id="name" name="first_name" placeholder="名" value="<?php if (!empty($clean['first_name'])) {
                                                                                                                                                            echo $clean['first_name'];
                                                                                                                                                        } ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- 会社名記入 -->
                                    <div class="form-heading">
                                        <label class="required">会社名</label>
                                        <div class="answer-box">
                                            <input class="form-control is_input" type="text" id="company-name" name="user_company" placeholder="例：株式会社○○" value="<?php if (!empty($clean['user_company'])) {
                                                                                                                                                                        echo $clean['user_company'];
                                                                                                                                                                    } ?>" required>
                                        </div>
                                    </div>

                                    <!-- 資本金記入 -->
                                    <div class="form-heading">
                                        <label class="required">資本金</label>
                                        <div class="answer-box">
                                            <select name="company_capital" id="company-capital" required>
                                                <?php if (!empty($company_capitals)) : ?>
                                                    <?php foreach ($company_capitals as $company_capital) : ?>
                                                        <option value="<?php echo $company_capital; ?>" <?php if (!empty($clean['company_capital']) && $clean['company_capital'] === $company_capital) {
                                                                                                            echo 'selected';
                                                                                                        } ?>><?php echo $company_capital; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <!--従業員数記入 -->
                                    <div class="form-heading">
                                        <label class="required">従業員数</label>
                                        <div class="answer-box">
                                            <input class="form-control is_input" type="text" id="number-of-employees" name="number_of_employees" placeholder="例：100" value="<?php if (!empty($clean['number_of_employees'])) {
                                                                                                                                                                                echo $clean['number_of_employees'];
                                                                                                                                                                            } ?>" required>
                                        </div>
                                    </div>

                                    <!-- メールアドレス記入 -->
                                    <div class="form-heading">
                                        <label class="required" for="mail">メールアドレス</label>
                                        <div class="answer-box">
                                            <input class="form-control is_input" type="text" id="mail" name="user_mail" placeholder="例：example@officestation.jp" value="<?php if (!empty($clean['user_mail'])) {
                                                                                                                                                                            echo $clean['user_mail'];
                                                                                                                                                                        } ?>" required>
                                        </div>
                                    </div>

                                    <!-- 電話番号記入 -->
                                    <div class="form-heading">
                                        <label class="required" for="tel">電話番号</label>
                                        <div class="answer-box">
                                            <input class="form-control" type="text" id="tel" name="user_tel" placeholder="例：000-000-0000" value="<?php if (!empty($clean['user_tel'])) {
                                                                                                                                                        echo $clean['user_tel'];
                                                                                                                                                    } ?>" required>
                                        </div>
                                    </div>

                                    <!-- 参加予定のセミナー記入 -->
                                    <div class="form-heading">
                                        <label class="required">参加予定のセミナー</label>
                                        <div class="answer-box">
                                            <select name="seminar" required="">
                                                <option value=""></option>
                                                <MTMultiBlog blog_ids="14">
                                                    <mt:Contents content_type="セミナー情報" sort_by="field:日付" sort_order="ascend">
                                                        <MTIgnore>変数定義</MTIgnore>
                                                        <MTSetVarBlock name="checkpro">
                                                            <mt:ContentField content_field="表示">
                                                                <mt:ContentFieldValue>
                                                            </mt:ContentField>
                                                        </MTSetVarBlock>
                                                        <MTSetVarBlock name="status">
                                                            <mt:ContentField content_field="ステータス">
                                                                <mt:ContentFieldValue>
                                                            </mt:ContentField>
                                                        </MTSetVarBlock>
                                                        <MTSetVarBlock name="target">
                                                            <mt:ContentField content_field="対象">
                                                                <mt:ContentFieldValue>
                                                            </mt:ContentField>
                                                        </MTSetVarBlock>
                                                        <MTSetVarBlock name="product">
                                                            <mt:ContentField content_field="製品">
                                                                <mt:ContentFieldValue>
                                                            </mt:ContentField>
                                                        </MTSetVarBlock>
                                                        <MTSetVarBlock name="dateTime">【<mt:ContentField content_field="日付">
                                                                <mt:ContentFieldValue format="%Y">/<mt:ContentFieldValue format="%b">/<mt:ContentFieldValue format="%e">
                                                                            <mt:ContentField content_field="時間" replace="～" ,"-">
                                                                                <mt:ContentFieldValue>
                                                                            </mt:ContentField>
                                                            </mt:ContentField>】</MTSetVarBlock>
                                                        <MTSetVarBlock name="title">
                                                            <mt:ContentLabel>
                                                        </MTSetVarBlock>
                                                        <MTIf name="target" eq="企業向け">
                                                            <MTIf name="status" ne="終了">
                                                                <MTIf name="product" eq="年末調整">
                                                                    <MTIf name="checkpro" ne="PRO用">
                                                                        <option value="<MTVar name='dateTime'> <MTVar name='title'>">
                                                                            <MTVar name="dateTime">
                                                                                <MTVar name="title">
                                                                        </option>
                                                                    </MTIf>
                                                                </MTIf>
                                                            </MTIf>
                                                        </MTIf>
                                                    </mt:Contents>
                                                </MTMultiBlog>
                                            </select>
                                        </div>
                                    </div>

                                    <!-- 広告アンケート記入 -->
                                    <div class="form-heading inflow_source">
                                        <label>「オフィスステーション 年末調整」を知った理由</label>
                                        <div class="answer-box">
                                            <?php if (!empty($inflow_sources)) : ?>
                                                <?php foreach ($inflow_sources as $id => $value) : ?>
                                                    <label for="<?php echo $id; ?>"><input id="<?php echo $id; ?>" type="radio" name="inflow_source" value="<?php echo $value; ?>" <?php if (!empty($clean['inflow_source']) && $clean['inflow_source'] === $value) {
                                                                                                                                                                                        echo 'checked';
                                                                                                                                                                                    } ?>><?php echo $value; ?></label>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                            <input type="text" name="othertext" id="othertext" value="<?php if (!empty($clean['othertext'])) {
                                                                                                            echo $clean['othertext'];
                                                                                                        } ?>">
                                        </div>
                                    </div>

                                    <!-- プライバシー同意 -->
                                    <div class="form-heading">
                                        <label class="required">プライバシーステートメントを確認し、同意します。</label>
                                        <div class="answer-box">
                                            <span class="value">
                                                <input type="checkbox" name="agree" id="agree" value="同意する" <?php if ($clean['agree'] === "同意する") {
                                                                                                                echo 'checked';
                                                                                                            } ?> required>
                                                <label class="agree" for="agree">同意する</label>
                                            </span>
                                            <p class="description"><a href="https://www.officestation.jp/privacy-statement/" target="_blank">プライバシーステートメント</a></p>
                                        </div>
                                    </div>
                                    <!-- バリデーションチェック用 -->
                                    <input type="hidden" name="agree_check">

                                    <!-- 私はロボットではありません -->
                                    <div class="g-recaptcha" data-callback="clearcall" data-sitekey="<?php echo $site_key ?>"></div>

                                    <!-- 確認画面へボタン -->
                                    <p class="submit text-center">
                                        <input id="os-submit" type="submit" name="btn_confirm" class="button" value="確認画面へ" disabled>
                                    </p>

                                </form>
                            </div>
                        </section>

                    <?php endif; ?>

                <?php elseif ($page_flag === 1) : ?>

                    <section id="form-bottom" class="l-container__fluid">
                        <div class="l-container">
                            <h2 class="l-container__title">お申込み内容のご確認</h2>
                            <form action="form-thanks.php" method="post" class="form c-form">
                                <!-- 氏名表示 -->
                                <div class="form-heading">
                                    <label class="required">氏名</label>
                                    <p><?php echo $clean['last_name'] . " " . $clean['first_name']; ?></p>
                                </div>

                                <!-- 会社名表示 -->
                                <div class="form-heading">
                                    <label class="required">会社名</label>
                                    <p><?php echo $clean['user_company']; ?></p>
                                </div>

                                <!-- 資本金表示 -->
                                <div class="form-heading">
                                    <label class="required">資本金</label>
                                    <p><?php echo $clean['company_capital']; ?></p>
                                </div>

                                <!-- 従業員数表示 -->
                                <div class="form-heading">
                                    <label class="required">従業員数</label>
                                    <p><?php echo $clean['number_of_employees']; ?></p>
                                </div>

                                <!-- メールアドレス表示 -->
                                <div class="form-heading">
                                    <label class="required" for="mail">メールアドレス</label>
                                    <p><?php echo $clean['user_mail']; ?></p>
                                </div>

                                <!-- 電話番号表示 -->
                                <div class="form-heading">
                                    <label class="required" for="tel">電話番号</label>
                                    <p><?php echo $clean['user_tel']; ?></p>
                                </div>

                                <!-- 参加予定のセミナー表示 -->
                                <div class="form-heading">
                                    <label class="required">参加予定のセミナー</label>
                                    <p><?php echo $clean['seminar']; ?></p>
                                </div>

                                <!-- 広告アンケート表示 -->
                                <div class="form-heading">
                                    <label class="" for="inflow_source">「オフィスステーション 年末調整」を知った理由</label>
                                    <p>
                                        <?php
                                        if (($clean['inflow_source'] === "その他") && (!empty($clean['othertext']))) {
                                            $inflow_source = "その他:" . $clean['othertext'];
                                            $clean['inflow_source'] = $inflow_source;
                                        }
                                        echo $clean['inflow_source'];
                                        ?>
                                    </p>
                                </div>

                                <!-- プライバシー同意 -->
                                <div class="form-heading">
                                    <label class="required" for="">プライバシーステートメントを確認し、同意します。</label>
                                    <p><?php echo $clean['agree']; ?></p>
                                </div>

                                <!-- ボタン -->
                                <div class="btns">
                                    <input class="button" type="button" name="btn_back" value="戻る" onclick="history.back()">
                                    <input id="os-submit" class="button" type="submit" name="btn_submit" value="送信する">
                                </div>

                                <!-- hiddenでサンクス画面へデータ引き渡し -->
                                <input type="hidden" name="last_name" value="<?php echo $clean['last_name']; ?>">
                                <input type="hidden" name="first_name" value="<?php echo $clean['first_name']; ?>">
                                <input type="hidden" name="user_company" value="<?php echo $clean['user_company']; ?>">
                                <input type="hidden" name="company_capital" value="<?php echo $clean['company_capital']; ?>">
                                <input type="hidden" name="number_of_employees" value="<?php echo $clean['number_of_employees']; ?>">
                                <input type="hidden" name="user_mail" value="<?php echo $clean['user_mail']; ?>">
                                <input type="hidden" name="user_tel" value="<?php echo $clean['user_tel']; ?>">
                                <input type="hidden" name="seminar" value="<?php echo $clean['seminar']; ?>">
                                <input type="hidden" name="inflow_source" value="<?php echo $clean['inflow_source']; ?>">
                                <input type="hidden" name="agree" value="<?php echo $clean['agree']; ?>">

                            </form>
                        </div>
                    </section>

                <?php endif; ?>


                <!--content-->
                <footer class="l-footer">
                    <div class="l-container l-container__flex u-pb0">
                        <div class="l-footer__logo">
                            <img src="/img/common/c_logo.png" alt="株式会社エフアンドエム">
                        </div>
                        <p class="l-footer__copy-right">&copy; 2020 F&M co.,ltd.</p>
                    </div>
                </footer>

                <script src="/module/form/recaptcha.js"></script>
                <script>
                    $(function() {
                        $('a[href^="#"]').click(function() {
                            var speed = 500;
                            var href = $(this).attr("href");
                            var target = $(href == "#" || href == "" ? 'html' : href);
                            //ヘッダーの高さを取得
                            var header = $('header').height();
                            //ヘッダーの高さを引く
                            var position = target.offset().top - header;
                            $("html, body").animate({
                                scrollTop: position
                            }, speed, "swing");
                            return false;
                        });
                    });
                </script>
                <script src="./dist/main.js"></script>
            </body>

            </html>
        </mt:Unless>
        <MTIgnore>不要なCMSからの空白改行を除去</MTIgnore>