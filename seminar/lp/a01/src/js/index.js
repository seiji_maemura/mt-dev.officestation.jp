/**
 * SASS Import
 */
import '../scss/app.scss';

/**
 * JS Import
 */
import { fixedFooter } from '../../../../../module/utility/footer.js';
fixedFooter();

import '../../../../../module/form/validation';
